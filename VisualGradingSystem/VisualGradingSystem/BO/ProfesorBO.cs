﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualGradingSystem.BO
{
    public class ProfesorBO
    {

        public ProfesorBO()
        {

        }
        private int idProfesor;

        public int IdProfesor
        {
            get { return idProfesor; }
            set { idProfesor = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string direccion;

        public string Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private string telefono;

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }
        private CuentaBO oCuenteBO;

        public CuentaBO OCuenteBO
        {
            get { return oCuenteBO; }
            set { oCuenteBO = value; }
        }
        private string estado;

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }
    }
}