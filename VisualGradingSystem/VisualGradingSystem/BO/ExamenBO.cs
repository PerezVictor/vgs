﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualGradingSystem.BO
{
    public class ExamenBO
    {
        public ExamenBO()
        {

        }
        private int idExamen;

        public int IdExamen
        {
            get { return idExamen; }
            set { idExamen = value; }
        }
        private string estado;

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }
        private MateriaBO oMateriaBO;

        public MateriaBO OMateriaBO
        {
            get { return oMateriaBO; }
            set { oMateriaBO = value; }
        }
        private ProfesorBO oProfesorBO;

        public ProfesorBO OProfesorBO
        {
            get { return oProfesorBO; }
            set { oProfesorBO = value; }
        }
    }
}