﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualGradingSystem.BO
{
    public class PreguntaBO
    {
        public PreguntaBO()
        {

        }
        private int idPregunta;

        public int IdPregunta
        {
            get { return idPregunta; }
            set { idPregunta = value; }
        }
        private string pregunta;

        public string Pregunta
        {
            get { return pregunta; }
            set { pregunta = value; }
        }
        private string respuestaA;

        public string RespuestaA
        {
            get { return respuestaA; }
            set { respuestaA = value; }
        }
        private string respuestaB;

        public string RespuestaB
        {
            get { return respuestaB; }
            set { respuestaB = value; }
        }
        private string respuestaC;

        public string RespuestaC
        {
            get { return respuestaC; }
            set { respuestaC = value; }
        }
        private string respuestaD;

        public string RespuestaD
        {
            get { return respuestaD; }
            set { respuestaD = value; }
        }
        private string respuesta;

        public string Respuesta
        {
            get { return respuesta; }
            set { respuesta = value; }
        }
        private string estado;

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }
        private ExamenBO oExamenBO;

        public ExamenBO OExamenBO
        {
            get { return oExamenBO; }
            set { oExamenBO = value; }
        }
    }
}