﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualGradingSystem.BO
{
    public class CuentaBO
    {

        public CuentaBO()
        {

        }
        private int idCuenta;

        public int IdCuenta
        {
            get { return idCuenta; }
            set { idCuenta = value; }
        }
        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private string contraseña;

        public string Contraseña
        {
            get { return contraseña; }
            set { contraseña = value; }
        }
        private string tipo;

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        private string estado;

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }


    }
}