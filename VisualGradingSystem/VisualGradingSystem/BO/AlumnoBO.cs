﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualGradingSystem.BO
{
    public class AlumnoBO
    {

        public AlumnoBO()
        {

        }
        private int idAlumno;

        public int IdAlumno
        {
            get { return idAlumno; }
            set { idAlumno = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private CuentaBO oCuentaBO;

        public CuentaBO OCuentaBO
        {
            get { return oCuentaBO; }
            set { oCuentaBO = value; }
        }
        private GradoBO oGradoBO;

        public GradoBO OGradoBO
        {
            get { return oGradoBO; }
            set { oGradoBO = value; }
        }
        private string estado;

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }
    }
}