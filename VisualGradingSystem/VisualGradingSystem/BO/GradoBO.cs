﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualGradingSystem.BO
{
    public class GradoBO
    {
        public GradoBO()
        {

        }
        private int idGrado;

        public int IdGrado
        {
            get { return idGrado; }
            set { idGrado = value; }
        }
        private string grado;

        public string Grado
        {
            get { return grado; }
            set { grado = value; }
        }
        private string grupo;

        public string Grupo
        {
            get { return grupo; }
            set { grupo = value; }
        }
        private string estado;

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }

    }
}