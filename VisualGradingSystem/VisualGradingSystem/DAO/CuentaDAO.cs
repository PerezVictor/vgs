﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VisualGradingSystem.BO;

namespace VisualGradingSystem.DAO
{
    public class CuentaDAO
    {
        Conexion con;
        DataSet ds = null;
        string sql;
        SqlCommand cmd;
        SqlDataAdapter da;

        public DataSet consultar(object obj)
        {
            string cadenaWhere = "";
            bool edo = false;

            CuentaBO bo = (CuentaBO)obj;

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (bo.IdCuenta > 0)
            {
                cadenaWhere = cadenaWhere + " IdCuenta=" + bo.IdCuenta + " and";
                edo = true;
            }
            if (bo.Usuario != null)
            {
                cadenaWhere = cadenaWhere + " Usuario='" + bo.Usuario + "' and";
                edo = true;
            }
            if (bo.Contraseña != null)
            {
                cadenaWhere = cadenaWhere + " Contraseña='" + bo.Contraseña + "' and";
                edo = true;
            }
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "Select * from Cuenta " + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarLike(object obj)
        {
            string cadenaWhere = "";
            bool edo = false;

            CuentaBO bo = (CuentaBO)obj;

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (bo.IdCuenta > 0)
            {
                cadenaWhere = cadenaWhere + " IdCuenta=" + bo.IdCuenta + " and";
                edo = true;
            }
            if (bo.Usuario != null)
            {
                cadenaWhere = cadenaWhere + " Usuario like '%" + bo.Usuario + "%' and";
                edo = true;
            }
            if (bo.Tipo != null)
            {
                cadenaWhere = cadenaWhere + " Tipo='" + bo.Tipo + "' and";
                edo = true;
            }
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "Select * from Cuenta " + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }

        public DataSet consultarMt()
        {

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();           

            sql = "Select IdCuenta, Usuario from Cuenta where Tipo = 'Maestro' and "
            + "Cuenta.IdCuenta not in ( "
            + "Select Cuenta.IdCuenta from Cuenta "
            + "inner join Profesor on Profesor.IdCuenta = Cuenta.IdCuenta where Tipo = 'Maestro')";

            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }

        public DataSet consultarDD(int id)
        {
            sql = "";
            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (id != 0)
            {
                sql = "select IdCuenta, Usuario from Cuenta where IdCuenta=" + id + " union ";
            }

            sql += "select null IdCuenta, 'Seleccione' Usuario from Cuenta union "
            + "Select IdCuenta, Usuario from Cuenta where Tipo = 'Alumno' and "
            + "Cuenta.IdCuenta not in ( "
            + "Select Cuenta.IdCuenta from Cuenta "
            + "inner join Alumno on Alumno.IdCuenta = Cuenta.IdCuenta where Tipo = 'Alumno')";

            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }

        public int crear(object obj)
        {
            CuentaBO bo = (CuentaBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Insert into Cuenta (Usuario, Contraseña, Tipo, Estado) " +
               "VALUES('" +
                bo.Usuario.Trim() + "','" +
                bo.Contraseña.Trim() + "','" +
                bo.Tipo.Trim() + "','" +
                bo.Estado.Trim() + "')";

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }

        public int modificar(object obj)
        {
            CuentaBO bo = (CuentaBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Update Cuenta set " +

                "Usuario='" + bo.Usuario.Trim() + "'," +
                "Contraseña='" + bo.Contraseña.Trim() + "'," +
                "Estado='" + bo.Estado + "' where idCuenta=" + bo.IdCuenta.ToString();

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }

        public int eliminar(object obj)
        {
            try
            {
                CuentaBO bo = (CuentaBO)obj;

                cmd = new SqlCommand();

                con = new Conexion();
                cmd.Connection = con.establecerConexion();
                con.AbrirConexion();

                sql = " Delete from Cuenta " +

                     " where idCuenta=" + bo.IdCuenta.ToString();

                cmd.CommandText = sql;

                int i = cmd.ExecuteNonQuery();
                con.CerrarConexcion();
                if (i <= 0)
                {
                    return 0;
                }
                return 1;
            }
            catch (SqlException Ex)
            {
                return 0;
            }
            
        }
    }
}