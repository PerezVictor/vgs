﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VisualGradingSystem.BO;

namespace VisualGradingSystem.DAO
{
    public class AlumnoDAO
    {
        Conexion con;
        DataSet ds = null;
        string sql;
        SqlCommand cmd;
        SqlDataAdapter da;

        public DataSet consultar(object obj)
        {
            string cadenaWhere = "";
            bool edo = false;
            sql = "";
            
            AlumnoBO bo = (AlumnoBO)obj;
            

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (bo.IdAlumno > 0)
            {
                cadenaWhere = cadenaWhere + " idAlumno=" + bo.IdAlumno + " and";
                edo = true;
            }
            if (bo.Nombre != null)
            {
                cadenaWhere = cadenaWhere + " Nombre like '%" + bo.Nombre + "%' and";
                edo = true;
            }
            if (bo.OGradoBO.IdGrado > 0)
            {
                cadenaWhere = cadenaWhere + " Grado.idGrado=" + bo.OGradoBO.IdGrado + " and";
                edo = true;
            }
            if (bo.OCuentaBO.IdCuenta > 0)
            {
                cadenaWhere = cadenaWhere + " Alumno.IdCuenta=" + bo.OCuentaBO.IdCuenta + " and";
                edo = true;
            }
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "Select * , Grado + '-' + Grupo GradoG from Alumno " 
            + "inner join Grado on Grado.idGrado = Alumno.idGrado "
            + "inner join Cuenta on Cuenta.IdCuenta = Alumno.IdCuenta " + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarCount()
        {

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " select count(*) cuenta from Alumno";
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }

        public int crear(object obj)
        {
            AlumnoBO bo = (AlumnoBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Insert into Alumno " +
               "VALUES('" +
                bo.Nombre.Trim() + "','" +
                bo.Email.Trim() + "','" +
                bo.Estado.Trim() + "','" +
                bo.OCuentaBO.IdCuenta + "','" +
                bo.OGradoBO.IdGrado + "')";

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }

        public int modificar(object obj)
        {
            AlumnoBO bo = (AlumnoBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Update Alumno set " +
                "Nombre='" + bo.Nombre.Trim() + "'," +
                "Email='" + bo.Email.Trim() + "'," +
                "IdCuenta='" + bo.OCuentaBO.IdCuenta + "'," +
                "IdGrado='" + bo.OGradoBO.IdGrado + "'," +
                "Estado='" + bo.Estado + "' where IdAlumno=" + bo.IdAlumno;

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }

        public int eliminar(object obj)
        {
            AlumnoBO bo = (AlumnoBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Delete from Alumno " +
                 " where IdAlumno=" + bo.IdAlumno;

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
    }
}