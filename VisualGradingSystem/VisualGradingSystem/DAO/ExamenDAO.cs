﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VisualGradingSystem.BO;

namespace VisualGradingSystem.DAO
{
    public class ExamenDAO
    {
        Conexion con;
        DataSet ds = null;
        string sql;
        SqlCommand cmd;
        SqlDataAdapter da;

        public ExamenDAO()
        {

        }

        public DataSet consultar(object obj)
        {
            string cadenaWhere = "";
            bool edo = false;


            ExamenBO bo = (ExamenBO)obj;


            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (bo.IdExamen > 0)
            {
                cadenaWhere = cadenaWhere + " IdExamen=" + bo.IdExamen + " and";
                edo = true;
            }            
            if (bo.OMateriaBO.IdMateria > 0)
            {
                cadenaWhere = cadenaWhere + " Materia.IdMateria=" + bo.OMateriaBO.IdMateria + " and";
                edo = true;
            }
            if (bo.OProfesorBO.IdProfesor > 0)
            {
                cadenaWhere = cadenaWhere + " Profesor.IdProfesor=" + bo.OProfesorBO.IdProfesor + " and";
                edo = true;
            }
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "select * from Examen "
            + "inner join Materia on Materia.IdMateria = Examen.IdMateria "
            + "inner join Profesor on Profesor.IdProfesor = Examen.IdProfesor " + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarCount(string IdProfesor)
        {

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " select count(*) cuenta from Examen inner join Profesor on Profesor.IdProfesor = Examen.IdProfesor where Profesor.IdProfesor=" + IdProfesor;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarPromedios(int IdProfesor)
        {

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Select Promedios.GradoG , AVG(Promedios.Calificacion) Promedio " +
            "from " +
            "( " +
            "select Alumno.IdAlumno, Grado + ' ' + Grupo as GradoG, SUM(Calificacion) as Calificacion, AlumnoExamen.IdExamen from AlumnoExamen " +
            "inner join Alumno on Alumno.IdAlumno = AlumnoExamen.IdAlumno " +
            "inner join Grado on Grado.IdGrado = Alumno.IdGrado " +
            "where IdProfesor = " + IdProfesor.ToString() +
            " Group by Grado.Grado, Grado.Grupo, Alumno.IdAlumno, AlumnoExamen.IdExamen " +
            ") Promedios " +
            "Group by Promedios.GradoG ";

            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarDD(int IdProfesor)
        {

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " select NULL IdExamen, 'Seleccione' Nombre from Examen union select Examen.IdExamen, Materia.Nombre from Examen inner join Materia on Examen.IdMateria = Materia.IdMateria where Examen.IdProfesor = " + IdProfesor;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarDD2(int IdAlumno)
        {

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " select NULL IdExamen, 'Seleccione' Nombre from Examen union select distinct Examen.IdExamen as IdExamen, Materia.Nombre as Nombre from AlumnoExamen inner join Examen on Examen.IdExamen = AlumnoExamen.IdExamen inner join Materia on Materia.IdMateria = Examen.IdMateria where AlumnoExamen.IdAlumno =" + IdAlumno;
            
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarReporte(string IdProfesor, string IdExamen, string IdGrado)
        {

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " select Profesor.IdProfesor, Profesor.Nombre Profesor, Examen.IdExamen, Materia.Nombre Materia, Grado.Grado + ' ' + Grado.Grupo Grado, Alumno.Nombre Alumno, Alumno.IdAlumno  " +
                " from Profesor, Materia, Grado, Alumno, Examen" +
                " where Grado.IdGrado = Alumno.IdGrado and Examen.IdMateria = Materia.IdMateria and Profesor.IdProfesor = " + IdProfesor + " and Examen.IdExamen = " + IdExamen + " and Grado.IdGrado = " + IdGrado;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public int crear(object obj)
        {
            ExamenBO bo = (ExamenBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Insert into Examen " +
               "VALUES('" +
                bo.Estado.Trim() + "'," +                
                bo.OMateriaBO.IdMateria + "," +
                bo.OProfesorBO.IdProfesor + ")";

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
        public int modificar(object obj)
        {
            ExamenBO bo = (ExamenBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Update Examen " +
               "set " +
                "Estado = '" + bo.Estado.Trim() + "'," +
                "IdMateria = " + bo.OMateriaBO.IdMateria + "," +
                "IdProfesor = " + bo.OProfesorBO.IdProfesor + " where IdExamen = " + bo.IdExamen;

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
        public int eliminar(object obj)
        {
            try
            {
                ExamenBO bo = (ExamenBO)obj;

                cmd = new SqlCommand();

                con = new Conexion();
                cmd.Connection = con.establecerConexion();
                con.AbrirConexion();

                sql = " Delete from Examen " +
                    " where IdExamen = " + bo.IdExamen;

                cmd.CommandText = sql;

                int i = cmd.ExecuteNonQuery();
                con.CerrarConexcion();
                if (i <= 0)
                {
                    return 0;
                }
                return 1;
            }
            catch (SqlException Ex)
            {
                return 0;
            }
            
        }
    }
}