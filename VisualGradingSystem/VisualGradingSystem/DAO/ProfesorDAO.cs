﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VisualGradingSystem.BO;

namespace VisualGradingSystem.DAO
{
    public class ProfesorDAO
    {
        Conexion con;
        DataSet ds = null;
        string sql;
        SqlCommand cmd;
        SqlDataAdapter da;

        public DataSet consultar(object obj)
        {
            string cadenaWhere = "";
            bool edo = false;

            ProfesorBO bo = (ProfesorBO)obj;

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (bo.IdProfesor > 0)
            {
                cadenaWhere = cadenaWhere + " IdProfesor=" + bo.IdProfesor + " and";
                edo = true;
            }
            if (bo.Nombre != null)
            {
                cadenaWhere = cadenaWhere + " Nombre='" + bo.Nombre + "' and";
                edo = true;
            }
            if (bo.Telefono != null)
            {
                cadenaWhere = cadenaWhere + " Telefono='" + bo.Telefono + "' and";
                edo = true;
            }
            if (bo.OCuenteBO.IdCuenta > 0)
            {
                cadenaWhere = cadenaWhere + " IdCuenta=" + bo.OCuenteBO.IdCuenta + " and";
                edo = true;
            }
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "Select * from Profesor " + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarDD()
        {
            string cadenaWhere = "";
            bool edo = false;            

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();
           
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "Select null IdProfesor, 'Seleccione' Nombre from Profesor union Select IdProfesor, Nombre from Profesor " + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }

        public int crear(object obj)
        {
            ProfesorBO bo = (ProfesorBO)obj;

            cmd = new SqlCommand();
            
            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Insert into Profesor " +
               "VALUES('" +
                bo.Nombre.Trim() + "','" +
                bo.Direccion.Trim() + "','" +
                bo.Email.Trim() + "','" +
                bo.Estado.ToString() + "','" +
                bo.Telefono.Trim() + "','" +
                bo.OCuenteBO.IdCuenta + "')";

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }

        public int modificar(object obj)
        {
            ProfesorBO bo = (ProfesorBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Update Profesor set " +

                "Nombre='" + bo.Nombre.Trim() + "'," +
                "Direccion='" + bo.Direccion.Trim() + "'," +
                "Email='" + bo.Email.Trim() + "'," +
                "Telefono='" + bo.Telefono.Trim() + "'," +
                "IdCuenta='" + bo.OCuenteBO.IdCuenta + "'," +
                "Estado='" + bo.Estado + "' where idProfesor=" + bo.IdProfesor.ToString();

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }

        public int eliminar(object obj)
        {
            ProfesorBO bo = (ProfesorBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Delete from Profesor " +

                 " where idProfesor=" + bo.IdProfesor.ToString();

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
    }
}