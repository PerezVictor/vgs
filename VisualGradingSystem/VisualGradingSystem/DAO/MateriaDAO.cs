﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VisualGradingSystem.BO;

namespace VisualGradingSystem.DAO
{
    public class MateriaDAO
    {
        Conexion con;
        DataSet ds = null;
        string sql;
        SqlCommand cmd;
        SqlDataAdapter da;

        public MateriaDAO()
        {

        }
        public DataSet consultar(object obj)
        {
            string cadenaWhere = "";
            bool edo = false;


            MateriaBO bo = (MateriaBO)obj;


            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (bo.IdMateria > 0)
            {
                cadenaWhere = cadenaWhere + " IdMateria=" + bo.IdMateria + " and";
                edo = true;
            }
            if (bo.Nombre != null)
            {
                cadenaWhere = cadenaWhere + " Nombre like '%" + bo.Nombre + "%' and";
                edo = true;
            }            
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "Select * from Materia "
             + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarDD()
        {
            string cadenaWhere = "";
            bool edo = false;            


            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();
            
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "select null IdMateria, 'Seleccione' Nombre from Grado union select IdMateria , Nombre from Materia ";
             

            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public int crear(object obj)
        {
            MateriaBO bo = (MateriaBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Insert into Materia " +
               "VALUES('" +
                bo.Nombre.Trim() + "','" +
                bo.Estado.Trim() + "')";                                

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
        public int modificar(object obj)
        {
            MateriaBO bo = (MateriaBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Update Materia " +
               "set " +
                "Nombre = '" + bo.Nombre.Trim() + "'," +
                "Estado = '" + bo.Estado.Trim() + "' where IdMateria=" + bo.IdMateria;

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
        public int eliminar(object obj)
        {
            try
            {
                MateriaBO bo = (MateriaBO)obj;

                cmd = new SqlCommand();

                con = new Conexion();
                cmd.Connection = con.establecerConexion();
                con.AbrirConexion();

                sql = " Delete from Materia " +
                    " where IdMateria=" + bo.IdMateria;

                cmd.CommandText = sql;

                int i = cmd.ExecuteNonQuery();
                con.CerrarConexcion();

                if (i <= 0)
                {
                    return 0;
                }
                return 1;

            }catch(SqlException Ex){
                return 0;
            }
            
            
        }
    }
}