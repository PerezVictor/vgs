﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VisualGradingSystem.BO;

namespace VisualGradingSystem.DAO
{
    public class GradoDAO
    {
        Conexion con;
        DataSet ds = null;
        string sql;
        SqlCommand cmd;
        SqlDataAdapter da;

        public DataSet consultar(object obj)
        {
            string cadenaWhere = "";
            bool edo = false;

            GradoBO bo = (GradoBO)obj;

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (bo.IdGrado > 0)
            {
                cadenaWhere = cadenaWhere + " IdGrado=" + bo.IdGrado + " and";
                edo = true;
            }
            if (bo.Grado != null)
            {
                cadenaWhere = cadenaWhere + " Grado like '%" + bo.Grado + "%' and";
                edo = true;
            }
            if (bo.Grupo != null)
            {
                cadenaWhere = cadenaWhere + " Grupo like '%" + bo.Grupo + "%' and";
                edo = true;
            }
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "Select IdGrado, Grado, Grupo, Estado from Grado " + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }        

        public DataSet consultarDD()
        {                        

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();
           
            sql = " select null IdGrado, 'Seleccione' Grado from Grado union Select IdGrado,  Grado  + ' - ' + CAST(Grupo as nvarchar) Grado from Grado";
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarCount()
        {

            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " select count(*) cuenta from Grado";
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }

        public int crear(object obj)
        {

            GradoBO bo = (GradoBO)obj;

            cmd = new SqlCommand();            

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Insert into Grado (Grado,Grupo,Estado) " +
               "VALUES('" +
                bo.Grado.Trim() + "','" +
                bo.Grupo.Trim() + "','" +
                bo.Estado + "')";

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }

        public int modificar(object obj)
        {

            GradoBO bo = (GradoBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Update Grado set " +
                "Grado='" + bo.Grado.Trim() + "'," +
                "Grupo='" + bo.Grupo.Trim() + "'," +
                "Estado='" + bo.Estado + "' where IdGrado=" + bo.IdGrado.ToString();

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }

        public int eliminar(object obj)
        {
            try
            {
                GradoBO bo = (GradoBO)obj;

                cmd = new SqlCommand();

                con = new Conexion();
                cmd.Connection = con.establecerConexion();
                con.AbrirConexion();

                sql = " Delete from Grado " +
                    " where IdGrado=" + bo.IdGrado.ToString();

                cmd.CommandText = sql;

                int i = cmd.ExecuteNonQuery();
                if (i <= 0)
                {
                    return 0;
                }
                return 1;
            }
            catch (SqlException Ex)
            {
                return 0;
            }
            
        }
    }
}