﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VisualGradingSystem.BO;

namespace VisualGradingSystem.DAO
{
    public class PreguntaDAO
    {
        Conexion con;
        DataSet ds = null;
        string sql;
        SqlCommand cmd;
        SqlDataAdapter da;

        public PreguntaDAO()
        {

        }
        public DataSet consultar(object obj)
        {
            string cadenaWhere = "";
            bool edo = false;


            PreguntaBO bo = (PreguntaBO)obj;


            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            if (bo.IdPregunta > 0)
            {
                cadenaWhere = cadenaWhere + " IdPregunta=" + bo.IdPregunta + " and";
                edo = true;
            }            
            if (bo.OExamenBO.IdExamen > 0)
            {
                cadenaWhere = cadenaWhere + " IdExamen=" + bo.OExamenBO.IdExamen + " and";
                edo = true;
            }
            if (edo == true)
            {
                cadenaWhere = " where " + cadenaWhere.Remove(cadenaWhere.Length - 3, 3);
            }

            sql = "Select * from Preguntas "
                + cadenaWhere;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public DataSet consultarR(int idAlumno, int idExamen)
        {            
            cmd = new SqlCommand();
            ds = new DataSet();
            da = new SqlDataAdapter();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = "select distinct * from AlumnoExamen where IdAlumno =" + idAlumno + " and IdExamen= " + idExamen;
            cmd.CommandText = sql;
            da.SelectCommand = cmd;
            da.Fill(ds);
            con.CerrarConexcion();
            return ds;
        }
        public int crear(object obj)
        {
            PreguntaBO bo = (PreguntaBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Insert into Preguntas " +
               "VALUES('" +
                bo.Pregunta.Trim() + "','" +
                bo.RespuestaA.Trim() + "','" +
                bo.RespuestaB.Trim() + "','" +
                bo.RespuestaC.Trim() + "','" +
                bo.RespuestaD.Trim() + "','" +
                bo.Respuesta.Trim() + "','" +
                bo.Estado.Trim() + "'," +                
                bo.OExamenBO.IdExamen + ")";

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
        public int modificar(object obj)
        {
            PreguntaBO bo = (PreguntaBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Update Preguntas " +
               "set " +
                "Pregunta ='" + bo.Pregunta.Trim() + "'," +
                "RespuestaA ='" + bo.RespuestaA.Trim() + "'," +
                "RespuestaB ='" + bo.RespuestaB.Trim() + "'," +
                "RespuestaC ='" + bo.RespuestaC.Trim() + "'," +
                "RespuestaD = '" + bo.RespuestaD.Trim() + "'," +
                "Respuesta = '" + bo.Respuesta.Trim() + "'," +
                "Estado ='" + bo.Estado.Trim() + "'," +
                "IdExamen =" + bo.OExamenBO.IdExamen + " where IdPregunta=" + bo.IdPregunta;

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
        public int modificarPre(int idPregunta)
        {
            

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Update alumnoexamen " +
               "set " +                
               "Calificacion = 0" + " where IdAlumnoExamen=" + idPregunta;

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
        public int eliminar(object obj)
        {
            PreguntaBO bo = (PreguntaBO)obj;

            cmd = new SqlCommand();

            con = new Conexion();
            cmd.Connection = con.establecerConexion();
            con.AbrirConexion();

            sql = " Delete from Preguntas " +
                " where IdPregunta=" + bo.IdPregunta;

            cmd.CommandText = sql;

            int i = cmd.ExecuteNonQuery();
            con.CerrarConexcion();
            if (i <= 0)
            {
                return 0;
            }
            return 1;
        }
    }
}