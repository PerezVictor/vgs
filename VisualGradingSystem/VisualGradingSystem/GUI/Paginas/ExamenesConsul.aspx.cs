﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class ExamenesConsul : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HiddenField1.Value = (string)Session["IdProfesor"];
            if (!IsPostBack)
            {
                llenarddMateria();
                buscar();
            }
            
        }
        private void llenarddMateria()
        {
            MateriaCTRL oMateriaDAO = new MateriaCTRL();
            ddMateria.DataSource = oMateriaDAO.consultarDD().Tables[0];
            ddMateria.DataBind();

        }
        private void buscar()
        {            
            ExamenBO oExamenBO = new ExamenBO();
            oExamenBO.OMateriaBO = new MateriaBO();
            oExamenBO.OProfesorBO = new ProfesorBO();
            ExamenCTRL oExamenDAO = new ExamenCTRL();

            string a = ddMateria.SelectedValue;

            if (ddMateria.SelectedValue != "")
            {
                oExamenBO.OMateriaBO.IdMateria = Convert.ToInt32(ddMateria.SelectedValue);
            }
            oExamenBO.OProfesorBO.IdProfesor = Convert.ToInt32(HiddenField1.Value);

            GrdBuscar.DataSource = oExamenDAO.consultar(oExamenBO).Tables[0];
            GrdBuscar.DataBind();
        }
        protected void GrdBuscar_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Detalle")
            {
                int indice = Convert.ToInt32(e.CommandArgument);

                int Id = (int)GrdBuscar.DataKeys[indice].Value;

                ExamenBO oExamenBO = new ExamenBO();
                oExamenBO.OMateriaBO = new MateriaBO();
                oExamenBO.OProfesorBO = new ProfesorBO();
                ExamenCTRL oExamenDAO = new ExamenCTRL();

                oExamenBO.IdExamen = Id;

                Session["Estado"] = "EX";
                Session["Tabla"] = oExamenDAO.consultar(oExamenBO).Tables[0];
                Response.Redirect("ExamenABC.aspx");
            }
            if (e.CommandName == "Ver")
            {
                int indice = Convert.ToInt32(e.CommandArgument);

                int Id = (int)GrdBuscar.DataKeys[indice].Value;

                Session["IdExamen"] = Convert.ToString(Id);
                
                Response.Redirect("PreguntasExamenes.aspx");
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            buscar();
        }
    }
}