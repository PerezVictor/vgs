﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class MaterConsul : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            buscar();
        }
        private void buscar()
        {
            MateriaBO oMateriaBO = new MateriaBO();
            MateriaCTRL oMateriaDAO = new MateriaCTRL();

            if (txtNombre.Text != "")
            {
                oMateriaBO.Nombre = txtNombre.Text;
            }


            GrdBuscar.DataSource = oMateriaDAO.consultar(oMateriaBO).Tables[0];
            GrdBuscar.DataBind();
        }
        protected void GrdBuscar_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Detalle")
            {
                int indice = Convert.ToInt32(e.CommandArgument);

                int Id = (int)GrdBuscar.DataKeys[indice].Value;

                MateriaBO oMateriaBO = new MateriaBO();
                MateriaCTRL oMateriaDAO = new MateriaCTRL();

                oMateriaBO.IdMateria = Id;

                Session["Estado"] = "M";
                Session["Tabla"] = oMateriaDAO.consultar(oMateriaBO).Tables[0];
                Response.Redirect("MateriaABC.aspx");
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            buscar();
        }
    }
}