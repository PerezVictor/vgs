﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="ExamenesConsul.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.ExamenesConsul" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Examen
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Examenes <small>Panel de Consultas</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i>Examenes
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <div role="form">
                        <label>Materia</label>
                        <div class="form-group input-group">
                            <asp:DropDownList ID="ddMateria" CssClass="form-control" runat="server" DataTextField="Nombre" DataValueField="IdMateria"></asp:DropDownList>
                            <span class="input-group-btn">
                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default" Text="Buscar" OnClick="LinkButton1_Click"><span class="fa fa-search "></span></asp:LinkButton></span>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">

                    <div role="form">

                        <div class="table-responsive">
                            <asp:GridView ID="GrdBuscar" CssClass=" table table-bordered table-hover table-striped " runat="server" DataKeyNames="IdExamen" OnRowCommand="GrdBuscar_RowCommand" AutoGenerateColumns="False">
                                <Columns>

                                    <asp:ButtonField ButtonType="Button" ControlStyle-CssClass="btn blue-grey white-text" CommandName="Detalle" Text="Seleccionar">
                                        <ControlStyle CssClass="btn blue-grey white-text"></ControlStyle>
                                    </asp:ButtonField>

                                    <asp:ButtonField ButtonType="Button" CommandName="Ver" Text="Ver Preguntas">
                                        <ControlStyle CssClass="btn blue-grey white-text" />
                                    </asp:ButtonField>

                                    <asp:BoundField DataField="Nombre" HeaderText="Materia" />
                                    <asp:CheckBoxField DataField="Estado" HeaderText="Estado" />

                                </Columns>
                            </asp:GridView>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
