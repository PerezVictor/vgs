﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="CalificacionesConsul.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.CalificacionesConsul" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Calificaciones <small>Panel de Consultas</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-search"></i> Calificaciones
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                        <form role="form">

                           <div class="form-group input-group">
                               <asp:TextBox ID="TextBox1"  class="form-control" runat="server"></asp:TextBox>
                                
                                   <span class="input-group-btn"><asp:LinkButton ID="LinkButton1"  runat="server" CssClass="btn btn-default" Text="Buscar"><span class="fa fa-search "></span></asp:LinkButton></span>
                            </div>

                            
                            </form>
                            </div>
                    </div>
                   <div class="row">
                    <div class="col-lg-12">

                        <form role="form">

                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" CssClass="table table-hover" runat="server"></asp:GridView>
                             
                            </div>

                            
                            </form>
                            </div>
                  
                </div>
        </div>
         </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
