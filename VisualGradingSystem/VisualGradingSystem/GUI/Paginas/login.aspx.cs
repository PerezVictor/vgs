﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private string Encriptar(string Pass)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();

            byte[] inputBytes = (new UnicodeEncoding()).GetBytes(Pass);
            byte[] hash = sha1.ComputeHash(inputBytes);

            return Convert.ToBase64String(hash);
        }
        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            CuentaBO oCuentaBO = new CuentaBO();
            CuentaCTRL oCuentaDAO = new CuentaCTRL();

            oCuentaBO.Usuario = txtUsuario.Text;
            oCuentaBO.Contraseña = Encriptar(txtContraseña.Text);

            if (oCuentaDAO.consultar(oCuentaBO).Tables[0].Rows.Count > 0)
            {
                if (oCuentaDAO.consultar(oCuentaBO).Tables[0].Rows[0][3].ToString() == "Maestro") 
                {
                    Session["TablaUser"] = oCuentaDAO.consultar(oCuentaBO).Tables[0];

                    ProfesorBO oProfesorBO = new ProfesorBO();
                    oProfesorBO.OCuenteBO = new CuentaBO();
                    ProfesorCTRL oProfesorDAO = new ProfesorCTRL();

                    oProfesorBO.OCuenteBO.IdCuenta = Convert.ToInt32(oCuentaDAO.consultar(oCuentaBO).Tables[0].Rows[0][0].ToString());

                    Session["TablaProfesor"] = oProfesorDAO.consultar(oProfesorBO).Tables[0];

                    Response.Redirect("Home.aspx");
                }
                if (oCuentaDAO.consultar(oCuentaBO).Tables[0].Rows[0][3].ToString() == "Alumno")
                {
                    Session["TablaUser"] = oCuentaDAO.consultar(oCuentaBO).Tables[0];

                    AlumnoBO oAlumnoBO = new AlumnoBO();
                    oAlumnoBO.OCuentaBO = new CuentaBO();
                    oAlumnoBO.OGradoBO = new GradoBO();
                    AlumnoCTRL oAlumnoCTRL = new AlumnoCTRL();

                    oAlumnoBO.OCuentaBO.IdCuenta = Convert.ToInt32(oCuentaDAO.consultar(oCuentaBO).Tables[0].Rows[0][0].ToString());

                    Session["TablaAlumno"] = oAlumnoCTRL.consultar(oAlumnoBO).Tables[0];

                    Response.Redirect("homeFront.aspx");
                }

            }
            else
            {
                Mensaje("Usuario o Contreseña incorrecta");
            }
        }
        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }
    }
}