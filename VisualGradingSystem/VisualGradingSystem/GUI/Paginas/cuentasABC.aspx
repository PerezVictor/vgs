﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="cuentasABC.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.cuentasABC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Cuentas
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Cuentas <small>Panel de Modificaciones</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Cuentas
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form">
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                            <div class="form-group">
                                <label>Usuario</label>
                                <asp:TextBox ID="txtUsuarioCuen" class="form-control" runat="server"></asp:TextBox>
                                
                                <p class="help-block">Ejemplo: "Nombre del Alumno + numero de lista "</p>
                            </div>

                            <div class="form-group">
                                <label>Contraseña</label>
                                <asp:TextBox ID="txtContraseñaCuen" class="form-control" type="password" placeholder="Contraseña" runat="server"></asp:TextBox>
                              
                            </div>                                                                                   

                            <div class="row">
                            <div class="col-lg-12">
                               <asp:Button ID="btnAgregar" CssClass=" btn fa-2x fa-lg white-text blue-grey darken-3" runat="server" Text="Agregar" OnClick="btnAgregar_Click" />
                                <asp:Button ID="btnModificar" CssClass="btn fa-2x fa-lg white-text blue-grey darken-3" Enabled="false" runat="server" Text="Modificar" OnClick="btnModificar_Click" />
                                <asp:Button ID="btnEliminar" CssClass="btn fa-2x fa-lg white-text red darken-3" Enabled="false" runat="server" Text="Eliminar" OnClick="btnEliminar_Click" />
                            </div>
                           
                            </div>
                            </form>
                            </div>
                    <div class="col-lg-1" >
                    </div>
                    <div class="col-lg-4" >
                       <span class="help-block text-justify"> Aqui usted tiene el control de<b> "Añadir, Eliminar y Modificar los Usuario de los alumnos"</b>, teniendo en cuenta que datos quiera realizar los cambios <b>'se recomienda'</b> no hacer cambios bruscos sin saber que modificar ya que puede dañar la misma informacion que usted esta manejando.</span>
                       </div>
                    </div>
                 <div class="col-lg-1" >
                    </div>
                </div>
        </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
