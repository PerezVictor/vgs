﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class cuentasABC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Recepcion();
            }
        }
        private void Recepcion()
        {
            string estado = "";
            string estado2 = "";

            estado = (string)Session["Estado"];
            estado2 = (string)Session["Estado2"];

            if (estado != null)
            {
                if (estado == "C")
                {
                    btnAgregar.Enabled = false;
                    btnEliminar.Enabled = true;
                    btnModificar.Enabled = true;

                    DataTable dt = (DataTable)Session["Tabla"];

                    HiddenField1.Value = dt.Rows[0][0].ToString();

                    txtUsuarioCuen.Text = dt.Rows[0][1].ToString();
                    txtContraseñaCuen.Text = "";

                    if (estado2 != null)
                    {
                        txtUsuarioCuen.Enabled = false;
                        btnEliminar.Enabled = false;
                    }
                }                                                
            }
        }
        private void Limpiar()
        {
            txtUsuarioCuen.Text = "";
            txtContraseñaCuen.Text = "";            
            Session["Estado"] = null;
        }
        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }
        private bool Validar()
        {
            string msj = "";

            if (txtUsuarioCuen.Text == "")
            {
                msj += "Usuairo \n";
            }
            if (txtContraseñaCuen.Text == "")
            {
                msj += "Contraseña \n";
            }            
            if (msj != "")
            {
                msj = "Falta \n" + msj;
                Mensaje(msj);
                return false;
            }
            else
            {
                return true;
            }
        }
        private string Encriptar(string Pass)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();

            byte[] inputBytes = (new UnicodeEncoding()).GetBytes(Pass);
            byte[] hash = sha1.ComputeHash(inputBytes);

            return Convert.ToBase64String(hash);
        }
        private void Crear()
        {
            CuentaBO oCuentaBO = new CuentaBO();
            CuentaCTRL oCuentaDAO = new CuentaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oCuentaBO.Usuario = txtUsuarioCuen.Text;
                oCuentaBO.Contraseña = Encriptar(txtContraseñaCuen.Text);
                oCuentaBO.Estado = "False";
                oCuentaBO.Tipo = "Alumno";

                int i = oCuentaDAO.crear(oCuentaBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se agrego correctamente");
                    Limpiar();
                    
                }

            }

        }
        private void Modificar()
        {
            string estado2 = "";
            CuentaBO oCuentaBO = new CuentaBO();
            CuentaCTRL oCuentaDAO = new CuentaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oCuentaBO.IdCuenta = Convert.ToInt32(HiddenField1.Value);
                oCuentaBO.Usuario = txtUsuarioCuen.Text;
                oCuentaBO.Contraseña = Encriptar(txtContraseñaCuen.Text);
                oCuentaBO.Estado = "False";

                string estado = "";

                estado = (string)Session["Estado2"];
                
                if (estado == "Alumno")
                {
                    oCuentaBO.Estado = "True";
                }
                
                oCuentaBO.Tipo = "Alumno";

                int i = oCuentaDAO.modificar(oCuentaBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    estado2 = (string)Session["Estado2"];

                    Mensaje("Se modifico correctamente");

                    if (estado2 == null)
                    {
                        Limpiar();
                        btnAgregar.Enabled = true;
                        btnEliminar.Enabled = false;
                        btnModificar.Enabled = false;
                    }                                        
                    
                }

            }

        }
        private void Elimnar()
        {
            CuentaBO oCuentaBO = new CuentaBO();
            CuentaCTRL oCuentaDAO = new CuentaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oCuentaBO.IdCuenta = Convert.ToInt32(HiddenField1.Value);                

                int i = oCuentaDAO.eliminar(oCuentaBO);
                if (i == 0)
                {
                    Mensaje("Error, Verifique si no hay un alumno utilizando esta cuenta");
                }
                else
                {
                    Mensaje("Se elimino correctamente");
                    Limpiar();
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false;
                }

            }

        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Crear();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Elimnar();
        }
    }
}