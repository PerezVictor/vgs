﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class AlumnosABC : System.Web.UI.Page
    {
        int id;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LlenarddGrado();
                LlenarddCuenta(0);
                Recepcion();
                
            }
            

        }

        private void LlenarddGrado()
        {
            GradoCTRL oGradoDAO = new GradoCTRL();
            ddGrado.DataSource = oGradoDAO.consultarDD().Tables[0];
            ddGrado.DataBind();
        }

        private void LlenarddCuenta(int id)
        {
            CuentaCTRL oCuentaDAO = new CuentaCTRL();
            ddCuenta.DataSource = oCuentaDAO.consultarDD(id).Tables[0];
            ddCuenta.DataBind();
        }

        private void Recepcion()
        {
            string estado = "";

            estado = (string)Session["Estado"];

            if (estado != null)
            {
                if (estado == "A")
                {
                    btnAgregar.Enabled = false;
                    btnEliminar.Enabled = true;
                    btnModificar.Enabled = true;

                    DataTable dt = (DataTable)Session["Tabla"];

                    id = Convert.ToInt32(dt.Rows[0][0].ToString());

                    txtNombre.Text = dt.Rows[0][1].ToString();
                    txtCorreo.Text = dt.Rows[0][2].ToString();
                    if (dt.Rows[0][3].ToString() == "True")
                    {
                        rbAc.Checked = true;
                    }
                    else
                    {
                        rbDe.Checked = true;
                    }

                    LlenarddCuenta(Convert.ToInt32(dt.Rows[0][4].ToString()));

                    ddCuenta.SelectedValue = dt.Rows[0][4].ToString();
                    ddGrado.SelectedValue = dt.Rows[0][5].ToString();
                }                
            }
        }

        private void Limpiar()
        {
            txtNombre.Text = "";
            txtCorreo.Text = "";
            ddCuenta.SelectedValue = "";
            ddGrado.SelectedValue = "";
            Session["Estado"] = null;
        }

        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }

        private bool Validar()
        {
            string msj = "";

            if (txtNombre.Text == "")
            {
                msj += "Nombre \n";
            }
            if (txtCorreo.Text == "")
            {
                msj += "Correo \n";
            }
            if (rbAc.Checked == false && rbDe.Checked == false)
            {
                msj += "Estado \n";
            }
            if (ddCuenta.SelectedValue == "")
            {
                msj += "Cuenta \n";
            }
            if (ddGrado.SelectedValue == "")
            {
                msj += "Grado y Grupo \n";
            }
            if (msj != "")
            {
                msj = "Falta \n" + msj;
                Mensaje(msj);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void Crear()
        {
            AlumnoBO oAlumnoBO = new AlumnoBO();
            oAlumnoBO.OGradoBO = new GradoBO();
            oAlumnoBO.OCuentaBO = new CuentaBO();
            AlumnoCTRL oAlumnoDAO = new AlumnoCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oAlumnoBO.Nombre = txtNombre.Text;
                oAlumnoBO.Email = txtCorreo.Text;

                if (rbAc.Checked == true)
                {
                    oAlumnoBO.Estado = "True";
                }
                else
                {
                    oAlumnoBO.Estado = "False";
                }
                oAlumnoBO.OCuentaBO.IdCuenta = Convert.ToInt32(ddCuenta.SelectedValue);
                oAlumnoBO.OGradoBO.IdGrado = Convert.ToInt32(ddGrado.SelectedValue);

                int i = oAlumnoDAO.crear(oAlumnoBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se agrego correctamente");
                    Limpiar();
                    LlenarddCuenta(0);
                }

            }



        }

        private void Modificar()
        {
            DataTable dt = (DataTable)Session["Tabla"];

            id = Convert.ToInt32(dt.Rows[0][0].ToString());

            AlumnoBO oAlumnoBO = new AlumnoBO();
            oAlumnoBO.OGradoBO = new GradoBO();
            oAlumnoBO.OCuentaBO = new CuentaBO();
            AlumnoCTRL oAlumnoDAO = new AlumnoCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oAlumnoBO.Nombre = txtNombre.Text;
                oAlumnoBO.Email = txtCorreo.Text;

                if (rbAc.Checked == true)
                {
                    oAlumnoBO.Estado = "True";
                }
                else
                {
                    oAlumnoBO.Estado = "False";
                }
                oAlumnoBO.OCuentaBO.IdCuenta = Convert.ToInt32(ddCuenta.SelectedValue);
                oAlumnoBO.OGradoBO.IdGrado = Convert.ToInt32(ddGrado.SelectedValue);
                oAlumnoBO.IdAlumno = id;

                int i = oAlumnoDAO.modificar(oAlumnoBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se modifico correctamente");
                    Limpiar();
                    LlenarddCuenta(0);
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false; 
                }

            }



        }

        private void Eliminar()
        {
            DataTable dt = (DataTable)Session["Tabla"];

            id = Convert.ToInt32(dt.Rows[0][0].ToString());

            AlumnoBO oAlumnoBO = new AlumnoBO();
            oAlumnoBO.OGradoBO = new GradoBO();
            oAlumnoBO.OCuentaBO = new CuentaBO();
            AlumnoCTRL oAlumnoDAO = new AlumnoCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oAlumnoBO.IdAlumno = id;
                
                int i = oAlumnoDAO.eliminar(oAlumnoBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se elimino correctamente");
                    Limpiar();
                    LlenarddCuenta(0);
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false; 
                }

            }



        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Crear();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
        }
    }
}