﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class singUpA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                Recepcion();
            }
            
        }
        private void Recepcion()
        {
            string Estado = (string)Session["Estado"];

            if (Estado != null)
            {
                if (Estado == "Sig")
                {
                    DataTable dtC = (DataTable)Session["TablaUser"];
                    DataTable dtP = (DataTable)Session["TablaProfesor"];
                    //Datos de su cuenta
                    txtUsuario.Text = dtC.Rows[0][1].ToString();
                    txtUsuario.Enabled = false;
                    //Datos del Profesor
                    txtNombre.Text = dtP.Rows[0][1].ToString();
                    txtDireccion.Text = dtP.Rows[0][2].ToString();
                    txtCorreo.Text = dtP.Rows[0][3].ToString();
                    txtTelefono.Text = dtP.Rows[0][5].ToString();

                }
            }
        }
        private void Limpiar()
        {
            txtContraseña.Text = "";
            txtCorreo.Text = "";
            txtDireccion.Text = "";
            txtNombre.Text = "";
            txtTelefono.Text = "";
            txtUsuario.Text = "";
        }

        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }
        private string Encriptar(string Pass)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();

            byte[] inputBytes = (new UnicodeEncoding()).GetBytes(Pass);
            byte[] hash = sha1.ComputeHash(inputBytes);

            return Convert.ToBase64String(hash);
        }
        private bool Validar()
        {
            string msj = "";

            if (txtNombre.Text == "")
            {
                msj += "Nombre \n";
            }
            if (txtDireccion.Text == "")
            {
                msj += "Direccion \n";
            }
            if (txtCorreo.Text == "")
            {
                msj += "Correo \n";
            }
            
            if (txtTelefono.Text == "")
            {
                msj += "Telefono \n";
            }
            if (txtUsuario.Text == "")
            {
                msj += "Usuario \n";
            }
            if (txtContraseña.Text == "")
            {
                msj += "Contreseña \n";
            }
            if (msj != "")
            {
                msj = "Falta \n" + msj;
                Mensaje(msj);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void Crear()
        {
            CuentaBO oCuentaBO = new CuentaBO();
            CuentaCTRL oCuentaDAO = new CuentaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oCuentaBO.Usuario = txtUsuario.Text;
                oCuentaBO.Contraseña = Encriptar(txtContraseña.Text);
                oCuentaBO.Estado = "True";
                oCuentaBO.Tipo = "Maestro";

                if (oCuentaDAO.consultar(oCuentaBO).Tables[0].Rows.Count == 0)
                {
                    int i = oCuentaDAO.crear(oCuentaBO);
                    if (i == 0)
                    {
                        Mensaje("Error");
                    }
                    else
                    {
                        ProfesorBO oProfesorBO = new ProfesorBO();
                        oProfesorBO.OCuenteBO = new CuentaBO();
                        ProfesorCTRL oProfesorDAO = new ProfesorCTRL();

                        oProfesorBO.Nombre = txtNombre.Text;
                        oProfesorBO.Direccion = txtDireccion.Text;
                        oProfesorBO.Email = txtCorreo.Text;
                        oProfesorBO.Telefono = txtTelefono.Text;
                        oProfesorBO.Estado = "True";
                        oProfesorBO.OCuenteBO.IdCuenta = Convert.ToInt32(oCuentaDAO.consultarMt().Tables[0].Rows[0][0].ToString());

                        int e = oProfesorDAO.crear(oProfesorBO);
                        if (e == 0)
                        {
                            Mensaje("Error");
                        }
                        else
                        {
                            Mensaje("Se agrego correctamente");
                            Limpiar();
                            Response.Redirect("login.aspx");
                        }
                        
                    }
                }
                else
                {
                    Mensaje("Ya existe el nombre de usuairo con el que desea acceder cambielo por favor");
                }
            }

        }
        private void Modificar()
        {
            DataTable dtC = (DataTable)Session["TablaUser"];
            DataTable dtP = (DataTable)Session["TablaProfesor"];

            CuentaBO oCuentaBO = new CuentaBO();
            CuentaCTRL oCuentaDAO = new CuentaCTRL();

            if (Validar() == false)
            {

            }
            else
            {

                oCuentaBO.IdCuenta = Convert.ToInt32(dtC.Rows[0][0].ToString());

                oCuentaBO.Usuario = txtUsuario.Text;
                oCuentaBO.Contraseña = Encriptar(txtContraseña.Text);
                oCuentaBO.Estado = "True";
                oCuentaBO.Tipo = "Maestro";
                                
                    int i = oCuentaDAO.modificar(oCuentaBO);
                    if (i == 0)
                    {
                        Mensaje("Error");
                    }
                    else
                    {
                        ProfesorBO oProfesorBO = new ProfesorBO();
                        oProfesorBO.OCuenteBO = new CuentaBO();
                        ProfesorCTRL oProfesorDAO = new ProfesorCTRL();

                        oProfesorBO.IdProfesor = Convert.ToInt32(dtP.Rows[0][0].ToString());
                        oProfesorBO.Nombre = txtNombre.Text;
                        oProfesorBO.Direccion = txtDireccion.Text;
                        oProfesorBO.Email = txtCorreo.Text;
                        oProfesorBO.Telefono = txtTelefono.Text;
                        oProfesorBO.Estado = "True";
                        oProfesorBO.OCuenteBO.IdCuenta = Convert.ToInt32(dtC.Rows[0][0].ToString());

                        int e = oProfesorDAO.modificar(oProfesorBO);
                        if (e == 0)
                        {
                            Mensaje("Error");
                        }
                        else
                        {
                            Session["Estado"] = null;
                            Mensaje("Se modifico correctamente");
                            Limpiar();
                            Response.Redirect("login.aspx");
                        }

                    }                
            }

        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            string Estado = (string)Session["Estado"];

            if (Estado != null)
            {
                if (Estado == "Sig")
                {
                    Modificar();
                }
            }
            else
            {
                Crear();
            }
            
        }
    }
}