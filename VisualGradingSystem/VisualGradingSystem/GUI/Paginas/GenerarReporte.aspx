﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="GenerarReporte.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.GenerarReporte" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     Reporte de Examenes
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Reportes <small>Generador de reportes</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-print"></i>Reporte de Examenes
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">

                    <div role="form">                                                                        
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <div class="form-group">
                            <label>Grado y Grupo</label>
                            <asp:DropDownList CssClass="form-control" ID="ddGrado" runat="server" DataTextField="Grado" DataValueField="IdGrado"></asp:DropDownList>

                        </div>
                        <div class="form-group">
                            <label>Examen</label>
                            <asp:DropDownList CssClass="form-control" ID="ddExamen" runat="server" DataTextField="Nombre" DataValueField="IdExamen"></asp:DropDownList>

                        </div>                                                
                        <div class="row">
                            <div class="col-lg-12">
                                <asp:Button ID="btnImprimir" CssClass=" btn fa-2x fa-lg white-text blue-grey darken-3" runat="server" Text="Imprimir" OnClick="btnImprimir_Click" />                                                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1">
                </div>
                <div class="col-lg-4">
                    <span class="help-block text-justify">Aqui usted tiene el control de<b> "La impresion de los formatos de examenes"</b>, teniendo en cuenta a que grado y grupo le quiere realizar el examen</span>
                </div>
            </div>
            <div class="col-lg-1">
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
