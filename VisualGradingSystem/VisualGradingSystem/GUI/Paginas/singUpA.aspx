﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterFrontEnd.Master" AutoEventWireup="true" CodeBehind="singUpA.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.singUpA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Registro
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
 <div class="colorful-page-wrapper">
  <div class="center-block">
    <div class="login-block">
      <form action="index.html" id="login-form" class="orb-form">
        <header>
          <div class="image-block"><img class="img-responsive img-rounded" src="../../Recursos/Imagenes/Sin titulo-1.png" alt="User" /></div>
          Registro <small>Bienvenido al Registro a VGS</small></header>
        <fieldset>
          <section>
            <div class="row">              
              <div class="col col-lg-12">
                  <div class="form-group">
                    <label>Nombre</label>
                     <asp:TextBox ID="txtNombre" class="form-control" runat="server"></asp:TextBox>                  
                  </div>               
              </div>
            </div>
          </section>
         
          <section>
            <div class="row">              
              <div class="col col-lg-12">
                  <div class="form-group">
                     <label>Direccion</label>
                      <asp:TextBox ID="txtDireccion" class="form-control" runat="server"></asp:TextBox>
                 </div>
              </div>
            </div>
          </section> 

           <section>
            <div class="row">              
              <div class="col col-lg-12">
                   <div class="form-group">
                     <label>Correo</label>
                     <asp:TextBox ID="txtCorreo" class="form-control" type="email" placeholder="example@example.com" runat="server"></asp:TextBox>
                  </div>
              </div>
            </div>
          </section> 

          <section>
            <div class="row">              
              <div class="col col-lg-12">
                 <div class="form-group">
                  <label>Telefono</label>
                   <asp:TextBox ID="txtTelefono" type="number" class="form-control" runat="server"></asp:TextBox>
                 </div>
              </div>
            </div>
          </section>
                        
          <section>
            <div class="row">              
              <div class="col col-lg-12">
               <div class="form-group">
                 <label>Usuario</label>
                  <asp:TextBox ID="txtUsuario" class="form-control" runat="server"></asp:TextBox>
                  <p class="help-block">Ejemplo: "El usuario no debe ser igual al Nombre "</p>
                </div>
              </div>
            </div>
          </section> 

             <section>
            <div class="row">              
              <div class="col col-lg-12">
                 <div class="form-group">
                   <label>Contraseña</label>
                   <asp:TextBox ID="txtContraseña" class="form-control" type="password" placeholder="Contraseña" runat="server"></asp:TextBox>
                 </div>
              </div>
            </div>
          </section>            
            <section>
            <div class="row">
              <div class="col col-lg-6"></div>
              <div class="col col-lg-6">
                  <asp:Button ID="btnRegistro" class="btn blue white-text btn-lg right-content" runat="server" Text="Aceptar" OnClick="btnRegistro_Click" />
               
              </div>
            </div>
          </section>
        </fieldset>
        <footer>
          
        </footer>
      </form>
    </div>
    <div class="using-social-header">Dudas y preguntas en las redes sociales</div>
    <div class="social-buttons">
      <ul class="social">
        <li><a href="http://facebook.com/"><i class="entypo-facebook-circled"></i></a></li>
        <li><a href="http://google.com/"><i class="entypo-gplus-circled"></i></a></li>
        <li><a href="http://twitter.com/"><i class="entypo-twitter-circled"></i></a></li>
      </ul>
    </div>
    <div class="copyrights"> Visual Grading System APS Template<br/>
      Creado por DazeinCreative &copy; 2014<br/>
        Modificado por APS &copy; 2016
    </div>
  </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <link href="../../Recursos/bootstrap%20login/css/styles.css" rel="stylesheet" />
    <script src="../../Recursos/bootstrap%20login/js/vendors/modernizr/modernizr.custom.js"></script>
    
    <script src="../../Recursos/bootstrap%20login/js/vendors/jquery/jquery-ui.min.js"></script>
    <script src="../../Recursos/bootstrap%20login/js/vendors/jquery/jquery.min.js"></script>
    <script src="../../Recursos/bootstrap%20login/js/vendors/forms/jquery.form.min.js"></script>
    <script src="../../Recursos/bootstrap%20login/js/vendors/forms/jquery.maskedinput.min.js"></script>
    <script src="../../Recursos/bootstrap%20login/js/vendors/forms/jquery.validate.min.js"></script>
    <script src="../../Recursos/bootstrap%20login/js/vendors/jquery-steps/jquery.steps.min.js"></script>
    <script src="../../Recursos/bootstrap%20login/js/vendors/nanoscroller/jquery.nanoscroller.min.js"></script>
    <script src="../../Recursos/bootstrap%20login/js/vendors/sparkline/jquery.sparkline.min.js"></script>
    <script src="../../Recursos/bootstrap%20login/js/scripts.js"></script>
   
</asp:Content>
