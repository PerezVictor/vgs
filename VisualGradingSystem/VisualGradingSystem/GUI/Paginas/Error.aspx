﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Error</title>
    <link href="../../Recursos/bootstrap%20frontEnd/css/bootstrap.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap%20frontEnd/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap%20frontEnd/css/style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Lo sentimos ha ocurrido un error</h1>

        <asp:Image ID="Image1" CssClass="img-responsive" runat="server" ImageUrl="~/Recursos/Imagenes/404.png" />

        <br />
        <br />        
    </div>
    </form>
</body>
</html>
