﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    DashBoard
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%=Chart%>
    <script src="../../Recursos/bootstrap%20admin/js/canvasjs.min.js"></script>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard <small>Estadisticas</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!-- /.row -->

            <div class="row">
                <%-- bloque 1  --%>
                <div class="col-lg-4 col-md-4">
                    <a href="#" style="color: white">
                        <div class="panel blue darken-3">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-sitemap fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                            <asp:Label ID="lbGrados" runat="server" Text="Label"></asp:Label>
                                        </div>
                                        <div>Grados y Grupos</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </a>
                </div>
                <%-- bloque 1  --%>
                <%-- bloque 2  --%>
                <div class="col-lg-4 col-md-4">
                    <a href="#" style="color: white">
                        <div class="panel green darken-2">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-group fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                            <asp:Label ID="lbAlumnos" runat="server" Text="Label"></asp:Label>
                                        </div>
                                        <div>Alumnos</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <%-- bloque 2  --%>
                <%-- bloque 3  --%>
                <div class="col-lg-4 col-md-4">
                    <a href="#" style="color: white">
                        <div class="panel yellow darken-3">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-file fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                            <asp:Label ID="lbExamenes" runat="server" Text="Label"></asp:Label>
                                        </div>
                                        <div>Examenes</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </a>
                </div>
                <%-- bloque 3  --%>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="responsive panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Promedio por grupos</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div class="col-lg-12">
                                    <div class="panel">
                                        <div id="chartPromedio" style="height: 400px; width: 100%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <%--<div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i>contenido</h3>
                        </div>
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i>2do Panel</h3>
                        </div>
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-money fa-fw"></i>3er panel</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>--%>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    <%--</div>--%>
    <!-- /#page-wrapper -->




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
