﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.SERVICE;
using System.Data;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class Home : System.Web.UI.Page
    {
        public string Chart;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TablaUser"] == null)
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                llenarContadores();
                GraficaPromedios();
            }            
        }
        private void llenarContadores()
        {
            //LLenar Grados y Grupos
            GradoCTRL oGradoDAO = new GradoCTRL();

            lbGrados.Text = oGradoDAO.consultarCount().Tables[0].Rows[0][0].ToString();

            //Llenar Alumnos
            AlumnoCTRL oAlumnoDAO = new AlumnoCTRL();

            lbAlumnos.Text = oAlumnoDAO.consultarCount().Tables[0].Rows[0][0].ToString();

            //Llenar Examenes
            ExamenCTRL oExamenDAO = new ExamenCTRL();

            DataTable dtP = (DataTable)Session["TablaProfesor"];

            lbExamenes.Text = oExamenDAO.consultarCount(dtP.Rows[0][0].ToString()).Tables[0].Rows[0][0].ToString();
        }
        private void GraficaPromedios()
        {
            string contenido = "";
            ExamenCTRL oExamenDAO = new ExamenCTRL();

            DataTable dtP = (DataTable)Session["TablaProfesor"];

            DataTable dt = oExamenDAO.consultarPromedios(Convert.ToInt32(dtP.Rows[0][0].ToString())).Tables[0];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                contenido = contenido + "{ y: " + dt.Rows[i][1].ToString() + ", label: \"" + dt.Rows[i][0].ToString() + "\" },";
                
            }

            Chart = Chart +	"<script type=\"text/javascript\">"+
		                    "window.onload = function () {"+
			                "var chartP = new CanvasJS.Chart(\"chartPromedio\", {"+
			            	"title: {"+
				           	"text: \"Promedios por Grupos\""+
				            "},"+
				            "data: [{"+
					        "type: \"column\","+
					        "dataPoints: ["+contenido.ToString()+
					        "]"+
				            "}]"+
			                "});"+
			                "chartP.render();"+
                            "}" +
                            "</script>";
        }

    }
}