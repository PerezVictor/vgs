﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="AlumConsul.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.AlumConsul" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Alumnos
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Alumnos <small>Panel de Consultas</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i>Alumnos
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">

                    <div role="form">
                        <label>Nombre</label>
                        <div class="form-group input-group">
                            <asp:TextBox ID="txtNombre" class="form-control" runat="server"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default" Text="Buscar" OnClick="LinkButton1_Click"><span class="fa fa-search "></span></asp:LinkButton></span>
                        </div>
                    </div>
                </div>                        
                <div class="col-lg-6">

                    <div role="form">
                        <label>Grado y Grupo</label>
                        <div class="form-group input-group">
                            <asp:DropDownList ID="ddGrado" CssClass="form-control" runat="server" DataTextField="Grado" DataValueField="IdGrado"></asp:DropDownList>
                            <span class="input-group-btn">
                                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-default" Text="Buscar" OnClick="LinkButton2_Click"><span class="fa fa-search "></span></asp:LinkButton></span>
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <div role="form">

                        <div class="table-responsive">
                            <asp:GridView ID="GrdBuscar" CssClass=" table table-bordered table-hover table-striped " runat="server" DataKeyNames="IdAlumno" OnRowCommand="GrdBuscar_RowCommand" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:ButtonField ButtonType="Button" ControlStyle-CssClass="btn blue-grey white-text" CommandName="Detalle" Text="Seleccionar" >
<ControlStyle CssClass="btn blue-grey white-text"></ControlStyle>
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="Email" HeaderText="Correo" />
                                    <asp:BoundField DataField="IdCuenta" HeaderText="IdCuenta" Visible="False" />
                                    <asp:BoundField DataField="Usuario" HeaderText="Usuario" />
                                    <asp:BoundField DataField="IdGrado" HeaderText="IdGrado" Visible="False" />
                                    <asp:BoundField DataField="GradoG" HeaderText="Grado y Grupo" />
                                    <asp:CheckBoxField DataField="Estado" HeaderText="Estado" Text="Activo" />
                                </Columns>
                            </asp:GridView>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
