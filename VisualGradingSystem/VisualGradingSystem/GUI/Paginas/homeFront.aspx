﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="homeFront.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.homeFront" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Examenes
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Examenes <small>Panel de Consulta</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i>Examenes
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div role="form">
                        <div class="form-group">
                            <label>Examenes</label>
                            <asp:DropDownList CssClass="form-control" ID="ddExamenesA" runat="server" DataTextField="Nombre" DataValueField="IdExamen"></asp:DropDownList>
                            <span class="input-group-btn">
                                <asp:LinkButton ID="btnBusExa" runat="server" CssClass="btn btn-default" Text="Buscar" OnClick="btnBusExa_Click"><span class="fa fa-search "></span></asp:LinkButton></span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Repeater ID="rpPreguntas" runat="server">
                <ItemTemplate>
                    <div class="row">
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-10">
                            <%# Eval("Pregunta") %>
                            <br />
                            Respuesta Correcta :
                        <%# Eval("Respuesta1") %>
                            <br />
                            Tu Respuesta :
                        <%# Eval("Respuesta2") %>
                            <br />
                            <br />
                            <br />
                        </div>

                    </div>
                </ItemTemplate>
            </asp:Repeater>

            <div class="row">
                <div class="col-lg-12">
                    <asp:Label ID="lbCali" Text="" runat="server"></asp:Label>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
