﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class GyGconsul : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                buscar();
            }
            
        }

        public void buscar()
        {
            GradoBO oGradoBO = new GradoBO();
            GradoCTRL oGradoDAO = new GradoCTRL();

            if (txtGrado.Text != "")
            {
                oGradoBO.Grado = txtGrado.Text;
            }
            if (txtGrupo.Text != "")
            {
                oGradoBO.Grupo = txtGrupo.Text;
            }

            GrdBuscar.DataSource = oGradoDAO.consultar(oGradoBO).Tables[0];
            GrdBuscar.DataBind();

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            buscar();
        }

        protected void GrdBuscar_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Detalle")
            {
                int indice = Convert.ToInt32(e.CommandArgument);

                int Id = (int)GrdBuscar.DataKeys[indice].Value;

                GradoBO oGradoBO = new GradoBO();
                GradoCTRL oGradoDAO = new GradoCTRL();

                oGradoBO.IdGrado = Id;

                DataTable dt = oGradoDAO.consultar(oGradoBO).Tables[0];
                Session["Estado"] = "GG";
                Session["Tabla"] = dt;
                Response.Redirect("GradoABC.aspx");
            }
        }
       
    }
}