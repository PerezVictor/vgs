﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="PreguntasExamenes.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.PreguntasExamenes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Agregar preguntas

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Preguntas <small>Panel de Consultas</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i>Lista de preguntas
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <form role="form">                        
                        <div class="row">
                            <div class="col-lg-12">
                                <asp:Button ID="btnAgregar" CssClass=" btn fa-2x fa-lg white-text blue-grey darken-3" runat="server" Text="Agregar Pregunta" OnClick="btnAgregar_Click" />                                                                
                                <asp:Button ID="btnRegresar" CssClass=" btn fa-2x fa-lg white-text blue-grey darken-3" runat="server" Text="Regresar" OnClick="btnRegresar_Click" />
                            </div>

                        </div>
                        <br />
                        <div class="form-group">
                            <asp:GridView ID="GrdBuscar" CssClass="table table-bordered table-hover table-striped" runat="server" AutoGenerateColumns="False" DataKeyNames="IdPregunta" OnRowCommand="GrdBuscar_RowCommand">
                                <Columns>
                                    <asp:ButtonField ButtonType="Button" CommandName="Detalle" Text="Seleccionar">
                                    <ControlStyle CssClass="btn blue-grey white-text" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="Pregunta" HeaderText="Pregunta" />
                                    <asp:BoundField DataField="Respuesta" HeaderText="Respuesta Correcta" />
                                </Columns>
                            </asp:GridView>
                        </div>                        
                    </form>
                </div>                
            </div>
            <div class="col-lg-1">
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
