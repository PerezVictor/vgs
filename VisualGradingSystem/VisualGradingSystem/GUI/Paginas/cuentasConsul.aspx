﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="cuentasConsul.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.cuentasConsul" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Cuentas
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cuentas <small>Panel de Consultas</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-search"></i>Cuentas
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">

                    <div role="form">
                        <label>Usuario</label>
                        <div class="form-group input-group">
                            <asp:TextBox ID="txtUsuario" class="form-control" runat="server"></asp:TextBox>

                            <span class="input-group-btn">
                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default" Text="Buscar" OnClick="LinkButton1_Click"><span class="fa fa-search "></span></asp:LinkButton></span>
                        </div>


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">

                    <div role="form">

                        <div class="table-responsive">
                            <asp:GridView ID="GrdBuscar" CssClass=" table table-bordered table-hover table-striped " runat="server" DataKeyNames="IdCuenta" OnRowCommand="GrdBuscar_RowCommand" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:ButtonField ButtonType="Button" ControlStyle-CssClass="btn blue-grey white-text" CommandName="Detalle" Text="Seleccionar">
                                        <ControlStyle CssClass="btn blue-grey white-text"></ControlStyle>
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="Usuario" HeaderText="Usuario" />
                                    <asp:CheckBoxField DataField="Estado" HeaderText="Estado" Text="Activo" />
                                </Columns>
                            </asp:GridView>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
