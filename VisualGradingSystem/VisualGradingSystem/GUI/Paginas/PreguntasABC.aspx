﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="PreguntasABC.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.ExamenesABC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Preguntas <small>Panel de Modificaciones</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i>Preguntas
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">

                    <form role="form">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <div class="form-group">
                            <label>Pregunta</label>
                            <asp:TextBox ID="txtPregunta" class="form-control" runat="server"></asp:TextBox>

                            <p class="help-block">Ejemplo: "¿En que año murio vangoh?"</p>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <asp:TextBox ID="txtA" class="form-control" type="text" placeholder="Respuesta A" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <asp:TextBox ID="txtB" class="form-control" type="text" placeholder="Respuesta B" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <asp:TextBox ID="txtC" class="form-control" type="text" placeholder="Respuesta C" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <asp:TextBox ID="txtD" class="form-control" type="text" placeholder="Respuesta D" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Respuesta Correcta</label>
                            <asp:DropDownList ID="ddRespuesta" class="form-control" runat="server">
                                <asp:ListItem>A</asp:ListItem>
                                <asp:ListItem>B</asp:ListItem>
                                <asp:ListItem>C</asp:ListItem>
                                <asp:ListItem>D</asp:ListItem>
                            </asp:DropDownList>                            
                        </div>
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                        <div class="row">
                            <div class="col-lg-12">
                                <asp:Button ID="btnAgregar" CssClass=" btn fa-2x fa-lg white-text blue-grey darken-3" runat="server" Text="Agregar" OnClick="btnAgregar_Click" />
                                <asp:Button ID="btnModificar" CssClass="btn fa-2x fa-lg white-text blue-grey darken-3" runat="server" Text="Modificar" Enabled="false" OnClick="btnModificar_Click" />
                                <asp:Button ID="btnEliminar" CssClass="btn fa-2x fa-lg white-text red darken-3" runat="server" Text="Eliminar" Enabled="false" OnClick="btnEliminar_Click" />
                            </div>

                        </div>
                    </form>
                </div>
                <div class="col-lg-1">
                </div>
                <div class="col-lg-4">
                    <span class="help-block text-justify">Aqui usted tiene el control de<b> "Añadir, Eliminar y Modificar los Datos a sus Preguntas"</b>, teniendo en cuenta que datos quiera realizar los cambios <b>'se recomienda'</b> no hacer cambios bruscos sin saber que modificar ya que puede dañar la misma informacion que usted esta manejando.</span>
                </div>
            </div>
            <div class="col-lg-1">
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
