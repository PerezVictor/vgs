﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas.Reportes
{
    public partial class VisorReporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GenerarReporte();
        }
        private void GenerarReporte()
        {
            DataTable dt = new DataTable();
            DataTable temp = new DataTable();

            temp = (DataTable)Session["Reporte"];

            dt.Columns.Add("Profesor", typeof(string));
            dt.Columns.Add("Materia", typeof(string));
            dt.Columns.Add("Grado", typeof(string));
            dt.Columns.Add("Alumno", typeof(string));
            dt.Columns.Add("QR", typeof(string));    
        
            //Contar las preguntas del examen
            PreguntaCTRL oPreguntaCTRL = new PreguntaCTRL();
            PreguntaBO oPreguntaBO = new PreguntaBO();
            oPreguntaBO.OExamenBO = new ExamenBO();

            oPreguntaBO.OExamenBO.IdExamen = Convert.ToInt32(temp.Rows[0][2].ToString());

            int coun = oPreguntaCTRL.consultar(oPreguntaBO).Tables[0].Rows.Count;           


            for (int i=0; i < temp.Rows.Count; i++)
            {
                //IdPro = 0, IdExa 2, IdAlu 6

                string ima = AddQR(temp.Rows[i][0].ToString(), temp.Rows[i][2].ToString(), temp.Rows[i][6].ToString(), "QR" + temp.Rows[i][0].ToString() + i, Convert.ToString(coun));

                dt.Rows.Add(temp.Rows[i][1].ToString(), temp.Rows[i][3].ToString(), temp.Rows[i][4].ToString(), temp.Rows[i][5].ToString(), ima);
            }

            ReportDocument rd = new ReportDocument();
            string path = Server.MapPath("~") + "GUI\\Paginas\\Reportes\\ReporteExa.rpt";

            rd.Load(path);
            rd.DataSourceConnections.Clear();
            rd.SetDataSource(dt);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, false, "Examenes");            
            //CrystalReportViewer1.RefreshReport();
        }
        private string AddQR(string IdProfesor, string IdExamen, string IdAlumno, string nombre, string preguntas)
        {
            QrEncoder Encoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();

            Encoder.TryEncode("P=" + IdProfesor + ",E=" + IdExamen + ",A=" + IdAlumno + ",C="+preguntas, out qrCode);

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedCodeSize(400,QuietZoneModules.Zero), Brushes.Black, Brushes.White);

            MemoryStream ms = new MemoryStream();

            renderer.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, ms);

            //Volver a byte
            //BinaryReader br = new BinaryReader(ms);
            //byte[] imagen = new byte[(int)ms.Length];
            //br.Read(imagen, 0, (int)ms.Length);

            //br.Close();
            

            
            var imagTemporal = new Bitmap(ms);
            var imagen = new Bitmap(imagTemporal, new Size(200, 200));

            imagen.Save(Server.MapPath("~") + "GUI\\Paginas\\Reportes\\" + "QR\\" + nombre + ".png", System.Drawing.Imaging.ImageFormat.Png);

            ms.Close();
            return Server.MapPath("~") + "GUI\\Paginas\\Reportes\\" + "QR\\" + nombre + ".png";
        }
    }
}