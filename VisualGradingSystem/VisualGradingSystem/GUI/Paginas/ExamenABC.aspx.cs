﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class ExamenABC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HiddenField2.Value = (string)Session["IdProfesor"];
                llenarddMateria();
                Recepcion();
            }
        }
        private void llenarddMateria()
        {
            MateriaCTRL oMateriaDAO = new MateriaCTRL();
            ddMateria.DataSource = oMateriaDAO.consultarDD().Tables[0];
            ddMateria.DataBind();

        }
        private void Recepcion()
        {
            string estado = "";

            estado = (string)Session["Estado"];

            if (estado != null)
            {
                if (estado == "EX")
                {
                    btnAgregar.Enabled = false;
                    btnEliminar.Enabled = true;
                    btnModificar.Enabled = true;

                    DataTable dt = (DataTable)Session["Tabla"];

                    HiddenField1.Value = dt.Rows[0][0].ToString();

                    if (dt.Rows[0][1].ToString() == "True")
                    {
                        RbAc.Checked = true;
                    }
                    else
                    {
                        RbDec.Checked = true;
                    }

                    ddMateria.SelectedValue = dt.Rows[0][2].ToString();
                }                                
            }
        }
        private void Limpiar()
        {
            
            ddMateria.SelectedValue = "";
            Session["Estado"] = null;
        }
        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }
        private bool Validar()
        {
            string msj = "";
            
            if (RbAc.Checked == false && RbDec.Checked == false)
            {
                msj += "Estado \n";
            }
            if (ddMateria.SelectedValue == "")
            {
                msj += "Materia \n";
            }            
            if (msj != "")
            {
                msj = "Falta \n" + msj;
                Mensaje(msj);
                return false;
            }
            else
            {
                return true;
            }
        }
        private void Crear()
        {
            ExamenBO oExamenBO = new ExamenBO();
            oExamenBO.OMateriaBO = new MateriaBO();
            oExamenBO.OProfesorBO = new ProfesorBO();
            ExamenCTRL oExamenDAO = new ExamenCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                
                if (RbAc.Checked == true)
                {
                    oExamenBO.Estado = "True";
                }
                else
                {
                    oExamenBO.Estado = "False";
                }

                oExamenBO.OMateriaBO.IdMateria = Convert.ToInt32(ddMateria.SelectedValue);
                oExamenBO.OProfesorBO.IdProfesor = Convert.ToInt32(HiddenField2.Value);

                int i = oExamenDAO.crear(oExamenBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se agrego correctamente");
                    Limpiar();                    
                }

            }



        }
        private void Modificar()
        {
            ExamenBO oExamenBO = new ExamenBO();
            oExamenBO.OMateriaBO = new MateriaBO();
            oExamenBO.OProfesorBO = new ProfesorBO();
            ExamenCTRL oExamenDAO = new ExamenCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oExamenBO.IdExamen = Convert.ToInt32(HiddenField1.Value);
                if (RbAc.Checked == true)
                {
                    oExamenBO.Estado = "True";
                }
                else
                {
                    oExamenBO.Estado = "False";
                }

                oExamenBO.OMateriaBO.IdMateria = Convert.ToInt32(ddMateria.SelectedValue);
                oExamenBO.OProfesorBO.IdProfesor = Convert.ToInt32(HiddenField2.Value);

                int i = oExamenDAO.modificar(oExamenBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se modifico correctamente");
                    Limpiar();
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false;
                }

            }



        }
        private void Eliminar()
        {
            ExamenBO oExamenBO = new ExamenBO();
            oExamenBO.OMateriaBO = new MateriaBO();
            oExamenBO.OProfesorBO = new ProfesorBO();
            ExamenCTRL oExamenDAO = new ExamenCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oExamenBO.IdExamen = Convert.ToInt32(HiddenField1.Value);

                int i = oExamenDAO.eliminar(oExamenBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se elimino correctamente");
                    Limpiar();
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false;
                }

            }



        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Crear();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
        }
    }
}