﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class ExamenesABC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HiddenField2.Value = (string)Session["IdExamen"];
            if (!IsPostBack)
            {
                Recepcion();
            }
        }
        private void Recepcion()
        {
            string estado = "";

            estado = (string)Session["Estado"];

            if (estado != null)
            {
                if (estado == "P")
                {
                    btnAgregar.Enabled = false;
                    btnEliminar.Enabled = true;
                    btnModificar.Enabled = true;

                    DataTable dt = (DataTable)Session["Tabla"];

                    HiddenField1.Value = dt.Rows[0][0].ToString();

                    txtPregunta.Text = dt.Rows[0][1].ToString().Trim();
                    txtA.Text = dt.Rows[0][2].ToString().Trim();
                    txtB.Text = dt.Rows[0][3].ToString().Trim();
                    txtC.Text = dt.Rows[0][4].ToString().Trim();
                    txtD.Text = dt.Rows[0][5].ToString().Trim();

                    ddRespuesta.SelectedValue = dt.Rows[0][6].ToString().Trim();
                }                
            }
        }
        private void Limpiar()
        {
            txtPregunta.Text = "";
            txtA.Text = "";
            txtA.Text = "";
            txtA.Text = "";
            txtA.Text = "";

            ddRespuesta.SelectedValue = "A";
            Session["Estado"] = null;

            Response.Redirect("PreguntasExamenes.aspx");
        }
        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }
        private bool Validar()
        {
            string msj = "";

            if (txtPregunta.Text == "")
            {
                msj += "Pregunta \n";
            }
            if (txtA.Text == "")
            {
                msj += "Respuesta A \n";
            }
            if (txtB.Text == "")
            {
                msj += "Respuesta B \n";
            }
            if (txtC.Text == "")
            {
                msj += "Respuesta C \n";
            }
            if (txtD.Text == "")
            {
                msj += "Respuesta D \n";
            }
            if (msj != "")
            {
                msj = "Falta \n" + msj;
                Mensaje(msj);
                return false;
            }
            else
            {
                return true;
            }
        }
        private void Crear()
        {
            PreguntaBO oPreguntaBO = new PreguntaBO();
            oPreguntaBO.OExamenBO = new ExamenBO();

            PreguntaCTRL oPreguntaDAO = new PreguntaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oPreguntaBO.Pregunta = txtPregunta.Text;
                oPreguntaBO.RespuestaA = txtA.Text;
                oPreguntaBO.RespuestaB = txtB.Text;
                oPreguntaBO.RespuestaC = txtC.Text;
                oPreguntaBO.RespuestaD = txtD.Text;

                oPreguntaBO.Respuesta = ddRespuesta.SelectedValue;

                oPreguntaBO.Estado = "True";
                oPreguntaBO.OExamenBO.IdExamen = Convert.ToInt32(HiddenField2.Value);

                int i = oPreguntaDAO.crear(oPreguntaBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se agrego correctamente");
                    Limpiar();                    
                }

            }
        }
        private void Modificar()
        {
            PreguntaBO oPreguntaBO = new PreguntaBO();
            oPreguntaBO.OExamenBO = new ExamenBO();

            PreguntaCTRL oPreguntaDAO = new PreguntaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oPreguntaBO.IdPregunta = Convert.ToInt32(HiddenField1.Value);

                oPreguntaBO.Pregunta = txtPregunta.Text;
                oPreguntaBO.RespuestaA = txtA.Text;
                oPreguntaBO.RespuestaB = txtB.Text;
                oPreguntaBO.RespuestaC = txtC.Text;
                oPreguntaBO.RespuestaD = txtD.Text;

                oPreguntaBO.Respuesta = ddRespuesta.SelectedValue;

                oPreguntaBO.Estado = "True";
                oPreguntaBO.OExamenBO.IdExamen = Convert.ToInt32(HiddenField2.Value);

                int i = oPreguntaDAO.modificar(oPreguntaBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se modifico correctamente");
                    Limpiar();
                }

            }
        }
        private void Eliminar()
        {
            PreguntaBO oPreguntaBO = new PreguntaBO();
            oPreguntaBO.OExamenBO = new ExamenBO();

            PreguntaCTRL oPreguntaDAO = new PreguntaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oPreguntaBO.IdPregunta = Convert.ToInt32(HiddenField1.Value);                                

                int i = oPreguntaDAO.eliminar(oPreguntaBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se elimino correctamente");
                    Limpiar();
                }

            }
        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Crear();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
        }
    }
}