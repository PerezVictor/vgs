﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class homeFront : System.Web.UI.Page
    {
        public string t;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LlenarExam();
            }

        }
        private void LlenarExam()
        {
            DataTable dtP = (DataTable)Session["TablaAlumno"];

            ExamenCTRL oExamenCTRL = new ExamenCTRL();
            ddExamenesA.DataSource = oExamenCTRL.consultarDD2(Convert.ToInt32(dtP.Rows[0][0].ToString())).Tables[0];
            ddExamenesA.DataBind();
        }
        protected void btnBusExa_Click(object sender, EventArgs e)
        {
            string a = ddExamenesA.SelectedValue;

            if (ddExamenesA.SelectedValue != "")
            {
                DataTable dtP = (DataTable)Session["TablaAlumno"];

                PreguntaBO oPreguntaBO = new PreguntaBO();
                oPreguntaBO.OExamenBO = new ExamenBO();
                PreguntaCTRL oPreguntaCTRL = new PreguntaCTRL();
                DataTable dt, dt2;

                oPreguntaBO.OExamenBO.IdExamen = Convert.ToInt32(ddExamenesA.SelectedValue);
                dt = oPreguntaCTRL.consultar(oPreguntaBO).Tables[0];
                dt2 = oPreguntaCTRL.consultarR(Convert.ToInt32(dtP.Rows[0][0].ToString()), Convert.ToInt32(ddExamenesA.SelectedValue)).Tables[0];

                LlenarRepiter(dt, dt2);
            }
            
        }
        private void LlenarRepiter(DataTable temp, DataTable temp2)
        {
            PreguntaCTRL oPreguntaCTRL = new PreguntaCTRL();

            int Calificacion = 0;
            DataTable dt = new DataTable();

            dt.Columns.Add("Pregunta", typeof(string));
            dt.Columns.Add("Respuesta1", typeof(string));
            dt.Columns.Add("Respuesta2", typeof(string));


            for (int i = 0; i < temp.Rows.Count; i++)
            {
                decimal v = Convert.ToDecimal( temp2.Rows[i][4].ToString());

                dt.Rows.Add(temp.Rows[i][1].ToString(), temp.Rows[i][6].ToString(), temp2.Rows[i][3].ToString());

                if (temp.Rows[i][6].ToString().Trim() == temp2.Rows[i][3].ToString().Trim())
                {
                    Calificacion += Convert.ToInt32(v);
                }
                else
                {
                    oPreguntaCTRL.modificarPre(Convert.ToInt32(temp2.Rows[i][0].ToString().Trim()));
                }                               
            }

            rpPreguntas.DataSource = dt;
            rpPreguntas.DataBind();

            lbCali.Text = "Calificacion : " + Convert.ToString(Calificacion);

        }
    }
}