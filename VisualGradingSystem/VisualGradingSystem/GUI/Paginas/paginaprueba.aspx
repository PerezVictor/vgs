﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="paginaprueba.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.paginaprueba" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Estadisticas</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                                
                <!-- /.row -->

                <div class="row">
                    <%-- bloque 1  --%>
                    <div class="col-lg-3 col-md-6">
                        <a href="#" style="color:white">
                        <div class="panel blue darken-2">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-users fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div></div>
                                        <div><h3>Alumnos</h3></div>
                                    </div>
                                </div>
                            </div>
                          
                        </div>
                       </a>
                    </div>
                     <%-- bloque 1  --%>
                     <%-- bloque 2  --%>
                    <div class="col-lg-3 col-md-6">
                         <a href="#" style="color:white">
                        <div class="panel green darken-2">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                            <asp:Label ID="labelTareas" runat="server" Text="Label"></asp:Label>
                                        </div>
                                        <div>Examenes</div>
                                    </div>
                                </div>
                            </div>                         
                        </div>
                    </a>
                    </div>
                    <%-- bloque 2  --%>
                    <%-- bloque 3  --%>
                    <div class="col-lg-3 col-md-6">
                        <a href="#" style="color:white">
                        <div class="panel yellow darken-3">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                        <asp:Label ID="contador2" runat="server" Text="Label"></asp:Label>    
                                        </div>
                                        <div>contador</div>
                                    </div>
                                </div>
                            </div>
                         
                        </div>
                       </a>
                    </div>
                    <%-- bloque 3  --%>
                    <%-- bloque 4  --%>
                    <div class="col-lg-3 col-md-6">
                        <a href="#" style="color:white">
                        <div class="panel cyan darken-2">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                             <asp:Label ID="contador3" runat="server" Text="Label"></asp:Label> 
                                        </div>
                                        <div>contador</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       </a>
                    </div>
                </div>
                <%-- bloque 4  --%>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="responsive panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Area Chart</h3>
                            </div>
                            <div class="panel-body">
                               <div id="chart" class="table-responsive">
       
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i>contenido</h3>
                            </div>
                            <div class="panel-body">
                                                        
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> 2do Panel</h3>
                            </div>
                            <div class="panel-body">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> 3er panel</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                   <%-- <table class="table table-bordered table-hover table-striped">--%>
                                        
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

</asp:Content>
