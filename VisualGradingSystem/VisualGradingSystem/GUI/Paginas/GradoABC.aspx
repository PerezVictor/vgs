﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Masters/MasterBackEnd.Master" AutoEventWireup="true" CodeBehind="GradoABC.aspx.cs" Inherits="VisualGradingSystem.GUI.Paginas.GradoABC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Grados y Grupos
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div id="page-wrapper">

        <div class="container-fluid">
            
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Grado y Grupo <small>Panel de Modificaciones</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i>Grados
                        </li>
                    </ol>
                </div>
            </div>                        
            <div class="row">
                <div class="col-lg-6">

                    <form role="form">

                        <div class="form-group">
                            <label>Grado</label>
                            <asp:TextBox ID="txtGrado" class="form-control" runat="server"></asp:TextBox>

                            <p class="help-block">Ejemplo: "grado 1, Primer grado."</p>
                        </div>

                        <div class="form-group">
                            <label>Grupo</label>
                            <asp:TextBox ID="txtGrupo" class="form-control" runat="server"></asp:TextBox>
                            <p class="help-block">Ejemplo: "Grupo 1, Grupo A, etc."</p>
                        </div>

                        <div class="form-group">
                            <label>Estado</label>
                            <br />
                            <asp:RadioButton ID="RbAc" runat="server" GroupName="grupo1" Text=" Activo" Checked="True" />
                            <br />
                            <asp:RadioButton ID="RbDec" GroupName="grupo1" runat="server" Text=" Inactivo" />

                        </div>

                        <div class="row">
                            <div class="col-lg-12">                                     
                                <asp:Button ID="btnAgregar" CssClass=" btn fa-2x fa-lg white-text blue-grey darken-3" runat="server" Text="Agregar" OnClick="btnAgregar_Click" />
                                <asp:Button ID="btnModificar" CssClass="btn fa-2x fa-lg white-text blue-grey darken-3" runat="server" Text="Modificar" Enabled="False" OnClick="btnModificar_Click" />
                                <asp:Button ID="btnEliminar" CssClass="btn fa-2x fa-lg white-text red darken-3" runat="server" Text="Eliminar" Enabled="False" OnClick="btnEliminar_Click" />
                            </div>

                        </div>
                    </form>
                </div>
                <div class="col-lg-1">
                </div>
                <div class="col-lg-4">
                    <span class="help-block text-justify">Aqui usted tiene el control de<b> "Añadir, Eliminar y Modificar los Grados"</b>, teniendo en cuenta que datos quiera realizar los cambios <b>'se recomienda'</b> no hacer cambios bruscos sin saber que modificar ya que puede dañar la misma informacion que usted esta manejando.</span>
                </div>
            </div>
            <div class="col-lg-1">
            </div>
        </div>
    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
