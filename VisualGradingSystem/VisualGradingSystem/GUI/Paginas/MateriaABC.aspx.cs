﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class MateriaABC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Recepcion();
            }
        }
        private void Recepcion()
        {
            string estado = "";

            estado = (string)Session["Estado"];

            if (estado != null)
            {
                if (estado == "M")
                {
                    btnAgregar.Enabled = false;
                    btnEliminar.Enabled = true;
                    btnModificar.Enabled = true;

                    DataTable dt = (DataTable)Session["Tabla"];

                    HiddenField1.Value = dt.Rows[0][0].ToString();

                    txtNombre.Text = dt.Rows[0][1].ToString();
                    if (dt.Rows[0][2].ToString() == "True")
                    {
                        rbAc.Checked = true;
                    }
                    else
                    {
                        rbDe.Checked = true;
                    }   
                }                                                            
            }
        }
        private void Limpiar()
        {
            txtNombre.Text = "";            
            Session["Estado"] = null;
        }
        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }
        private bool Validar()
        {
            string msj = "";

            if (txtNombre.Text == "")
            {
                msj += "Nombre \n";
            }            
            if (rbAc.Checked == false && rbDe.Checked == false)
            {
                msj += "Estado \n";
            }            
            if (msj != "")
            {
                msj = "Falta \n" + msj;
                Mensaje(msj);
                return false;
            }
            else
            {
                return true;
            }
        }
        private void Crear()
        {
            MateriaBO oMateriaBO = new MateriaBO();
            MateriaCTRL oMateriaDAO = new MateriaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oMateriaBO.Nombre = txtNombre.Text;
                

                if (rbAc.Checked == true)
                {
                    oMateriaBO.Estado = "True";
                }
                else
                {
                    oMateriaBO.Estado = "False";
                }

                int i = oMateriaDAO.crear(oMateriaBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se agrego correctamente");
                    Limpiar();                    
                }

            }



        }
        private void Modificar()
        {
            MateriaBO oMateriaBO = new MateriaBO();
            MateriaCTRL oMateriaDAO = new MateriaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oMateriaBO.IdMateria = Convert.ToInt32(HiddenField1.Value);
                oMateriaBO.Nombre = txtNombre.Text;


                if (rbAc.Checked == true)
                {
                    oMateriaBO.Estado = "True";
                }
                else
                {
                    oMateriaBO.Estado = "False";
                }

                int i = oMateriaDAO.modificar(oMateriaBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se modifico correctamente");
                    Limpiar();
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false;
                }

            }



        }
        private void Eliminar()
        {
            MateriaBO oMateriaBO = new MateriaBO();
            MateriaCTRL oMateriaDAO = new MateriaCTRL();

            if (Validar() == false)
            {

            }
            else
            {
                oMateriaBO.IdMateria = Convert.ToInt32(HiddenField1.Value);
                oMateriaBO.Nombre = txtNombre.Text;


                if (rbAc.Checked == true)
                {
                    oMateriaBO.Estado = "True";
                }
                else
                {
                    oMateriaBO.Estado = "False";
                }

                int i = oMateriaDAO.eliminar(oMateriaBO);
                if (i == 0)
                {
                    Mensaje("Error; Compruebe si no hay un Examen Enlazado con esta materia");
                }
                else
                {
                    Mensaje("Se elimino correctamente");
                    Limpiar();
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false;
                }

            }



        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Crear();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
        }
    }
}