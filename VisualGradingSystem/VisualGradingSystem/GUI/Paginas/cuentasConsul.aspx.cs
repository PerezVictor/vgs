﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class cuentasConsul : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                buscar();
            }
        }
        private void buscar()
        {
            CuentaBO oCuentaBO = new CuentaBO();
            CuentaCTRL oCuentaDAO = new CuentaCTRL();

            if (txtUsuario.Text != "")
            {
                oCuentaBO.Usuario = txtUsuario.Text;
            }
            oCuentaBO.Tipo = "Alumno";

            GrdBuscar.DataSource = oCuentaDAO.consultarLike(oCuentaBO).Tables[0];
            GrdBuscar.DataBind();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        protected void GrdBuscar_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Detalle")
            {
                int indice = Convert.ToInt32(e.CommandArgument);

                int Id = (int)GrdBuscar.DataKeys[indice].Value;

                CuentaBO oCuentaBO = new CuentaBO();
                CuentaCTRL oCuentaDAO = new CuentaCTRL();

                oCuentaBO.IdCuenta = Id;

                Session["Estado"] = "C";
                Session["Tabla"] = oCuentaDAO.consultar(oCuentaBO).Tables[0];
                Response.Redirect("CuentasABC.aspx");
            }
        }
    }
}