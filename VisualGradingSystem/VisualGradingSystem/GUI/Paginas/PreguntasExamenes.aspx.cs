﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class PreguntasExamenes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HiddenField1.Value = (string)Session["IdExamen"];
            buscar();
        }
        private void buscar()
        {
            PreguntaBO oPreguntaBO = new PreguntaBO();
            oPreguntaBO.OExamenBO = new ExamenBO();

            PreguntaCTRL oPreguntaDAO = new PreguntaCTRL();


            oPreguntaBO.OExamenBO.IdExamen = Convert.ToInt32(HiddenField1.Value);

            GrdBuscar.DataSource = oPreguntaDAO.consultar(oPreguntaBO).Tables[0];
            GrdBuscar.DataBind();
        }
        protected void GrdBuscar_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Detalle")
            {
                int indice = Convert.ToInt32(e.CommandArgument);

                int Id = (int)GrdBuscar.DataKeys[indice].Value;

                PreguntaBO oPreguntaBO = new PreguntaBO();
                oPreguntaBO.OExamenBO = new ExamenBO();

                PreguntaCTRL oPreguntaDAO = new PreguntaCTRL();

                oPreguntaBO.IdPregunta = Id;

                Session["Estado"] = "P";
                Session["Tabla"] = oPreguntaDAO.consultar(oPreguntaBO).Tables[0];
                Response.Redirect("PreguntasABC.aspx");
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("PreguntasABC.aspx");
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            Response.Redirect("ExamenesConsul.aspx");
        }
    }
}