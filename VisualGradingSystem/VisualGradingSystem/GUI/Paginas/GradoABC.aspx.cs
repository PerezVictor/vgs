﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class GradoABC : System.Web.UI.Page
    {
        int id = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                Recepcion();
            }
            
        }

        private void Recepcion()
        {
            string estado = "";

            estado = (string)Session["Estado"];

            if (estado != null)
            {
                if (estado == "GG")
                {
                    btnAgregar.Enabled = false;
                    btnEliminar.Enabled = true;
                    btnModificar.Enabled = true;

                    DataTable dt = (DataTable)Session["Tabla"];

                    id = Convert.ToInt32(dt.Rows[0]["IdGrado"].ToString());
                    txtGrado.Text = dt.Rows[0]["Grado"].ToString();
                    txtGrupo.Text = dt.Rows[0]["Grupo"].ToString();
                    if (dt.Rows[0]["Estado"].ToString() == "True")
                    {
                        RbAc.Checked = true;
                    }
                    else
                    {
                        RbDec.Checked = true;
                    }
                }                

            }
        }

        private void Limpiar()
        {
            txtGrado.Text = "";
            txtGrupo.Text = "";
            Session["Estado"] = null;
        }

        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }

        private bool Validar()
        {
            string msj = "";

            if (txtGrado.Text == "")
            {
                msj += "Grado \n";
            }
            if (txtGrupo.Text == "")
            {
                msj += "Grupo \n";
            }
            if (RbAc.Checked == false && RbDec.Checked == false)
            {
                msj += "Estado \n";
            }
            if (msj != "")
            {
                msj = "Falta \n" + msj;
                Mensaje(msj);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void Crear()
        {
            GradoBO oGradoBO = new GradoBO();
            GradoCTRL oGradoDAO = new GradoCTRL();
            if (Validar() == false)
            {

            }
            else
            {
                oGradoBO.Grado = txtGrado.Text;
                oGradoBO.Grupo = txtGrupo.Text;
                if (RbAc.Checked == true)
                {
                    oGradoBO.Estado = "True";
                }
                else
                {
                    oGradoBO.Estado = "False";
                }

                int i = oGradoDAO.crear(oGradoBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se agrego correctamente");
                    Limpiar();
                }
            }            

        }

        private void Modificar()
        {
            DataTable dt = (DataTable)Session["Tabla"];

            id = Convert.ToInt32(dt.Rows[0]["IdGrado"].ToString());

            GradoBO oGradoBO = new GradoBO();
            GradoCTRL oGradoDAO = new GradoCTRL();
            if (Validar() == false)
            {

            }
            else
            {
                oGradoBO.IdGrado = id;
                oGradoBO.Grado = txtGrado.Text;
                oGradoBO.Grupo = txtGrupo.Text;
                if (RbAc.Checked == true)
                {
                    oGradoBO.Estado = "True";
                }
                else
                {
                    oGradoBO.Estado = "False";
                }

                int i = oGradoDAO.modificar(oGradoBO);
                if (i == 0)
                {
                    Mensaje("Error");
                }
                else
                {
                    Mensaje("Se modifico correctamente");
                    Limpiar();
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false;                    
                }
            }  
        }

        private void Eliminar()
        {
            DataTable dt = (DataTable)Session["Tabla"];

            id = Convert.ToInt32(dt.Rows[0]["IdGrado"].ToString());

            GradoBO oGradoBO = new GradoBO();
            GradoCTRL oGradoDAO = new GradoCTRL();
            if (Validar() == false)
            {

            }
            else
            {
                oGradoBO.IdGrado = id;
                

                int i = oGradoDAO.eliminar(oGradoBO);
                if (i == 0)
                {
                    Mensaje("Error, Antes de Eliminar Verifique que no haya alumnos en este GRADO-GRUPO");
                }
                else
                {
                    Mensaje("Se elimino correctamente");
                    Limpiar();
                    btnAgregar.Enabled = true;
                    btnEliminar.Enabled = false;
                    btnModificar.Enabled = false;
                }
            }  
        }        

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Crear();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
        }        
  
    }
}