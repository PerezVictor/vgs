﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class GenerarReporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HiddenField1.Value = (string)Session["IdProfesor"];
                LlenarddGrado();
                LlenarddExamen();
            }
            
        }
        private void Mensaje(string ex)
        {
            string mensaje = ex;
            mensaje = mensaje.Replace(Environment.NewLine, "\\n");
            mensaje = mensaje.Replace("\n", "\\n");
            mensaje = mensaje.Replace("'", "\"");
            ClientScript.RegisterClientScriptBlock(typeof(Page), "Error", "<script> alert('" + mensaje + "');</script>");
        }
        private void LlenarddGrado()
        {
            GradoCTRL oGradoDAO = new GradoCTRL();
            ddGrado.DataSource = oGradoDAO.consultarDD().Tables[0];
            ddGrado.DataBind();
        }
        private void LlenarddExamen()
        {
            ExamenCTRL oExamenCTRL = new ExamenCTRL();
            ddExamen.DataSource = oExamenCTRL.consultarDD(Convert.ToInt32(HiddenField1.Value)).Tables[0];
            ddExamen.DataBind();
        }
        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (ddExamen.SelectedValue != "" && ddGrado.SelectedValue != "")
                {
                    ExamenCTRL oExamenCTRL = new ExamenCTRL();

                    Session["Reporte"] = oExamenCTRL.consultarReporte(HiddenField1.Value, ddExamen.SelectedValue, ddGrado.SelectedValue).Tables[0];

                    Response.Redirect("Reportes/VisorReporte.aspx");
                }
                else
                {
                    Mensaje("Seleccione un Examen y un Grado.");
                }
            }                        
        }
    }
}