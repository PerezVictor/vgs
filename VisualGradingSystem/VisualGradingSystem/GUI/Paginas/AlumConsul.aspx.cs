﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.SERVICE;

namespace VisualGradingSystem.GUI.Paginas
{
    public partial class AlumConsul : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                llenarddGrado();
                buscar();
            }
        }

        private void llenarddGrado()
        {
            GradoCTRL oGradoDAO = new GradoCTRL();
            ddGrado.DataSource = oGradoDAO.consultarDD().Tables[0];            
            ddGrado.DataBind();

            
        }

        private void buscar()
        {
            AlumnoBO oAlumnoBO = new AlumnoBO();
            oAlumnoBO.OGradoBO = new GradoBO();
            oAlumnoBO.OCuentaBO = new CuentaBO();
            AlumnoCTRL oAlumnoDAO = new AlumnoCTRL();

            if (txtNombre.Text != "")
            {
                oAlumnoBO.Nombre = txtNombre.Text;
            }
            if (ddGrado.Text != "")
            {
                oAlumnoBO.OGradoBO.IdGrado = Convert.ToInt32(ddGrado.SelectedValue);
            }

            GrdBuscar.DataSource = oAlumnoDAO.consultar(oAlumnoBO).Tables[0];
            GrdBuscar.DataBind();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            buscar();
        }

        protected void GrdBuscar_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Detalle")
            {
                int indice = Convert.ToInt32(e.CommandArgument);

                int Id = (int)GrdBuscar.DataKeys[indice].Value;

                AlumnoBO oAlumnoBO = new AlumnoBO();
                oAlumnoBO.OGradoBO = new GradoBO();
                oAlumnoBO.OCuentaBO = new CuentaBO();
                AlumnoCTRL oAlumnoDAO = new AlumnoCTRL();

                oAlumnoBO.IdAlumno = Id;
                
                Session["Estado"] = "A";
                Session["Tabla"] = oAlumnoDAO.consultar(oAlumnoBO).Tables[0]; 
                Response.Redirect("AlumnosABC.aspx");
            }
        }
    }
}