﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualGradingSystem.BO;
using VisualGradingSystem.DAO;


namespace VisualGradingSystem.GUI.Masters
{
    public partial class MasterBackEnd : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TablaUser"] == null)
            {
                Response.Redirect("login.aspx");
            }
            else
            {                

                if (Session["TablaProfesor"] != null)
                {
                    DataTable dtP = (DataTable)Session["TablaProfesor"];

                    HiddenField1.Value = dtP.Rows[0][0].ToString();
                    lbNombre.Text = dtP.Rows[0][1].ToString();
                    btnPerfil.Visible = true;
                }
                else
                {
                    DataTable dtP = (DataTable)Session["TablaAlumno"];

                    HiddenField1.Value = dtP.Rows[0][0].ToString();
                    lbNombre.Text = dtP.Rows[0][1].ToString();

                    btnVCuentas.Visible = false;
                    btnEAlumnos.Visible = false;
                    btnVAlumnos.Visible = false;
                    btnEGrados.Visible = false;
                    btnVGrados.Visible = false;

                    btnEMaterias.Visible = false;
                    btnVMaterias.Visible = false;
                    btnCExamen.Visible = false;
                    btnVExamen.Visible = false;
                    btnImprimirExa.Visible = false;

                    btnRevision.Visible = true;
                }
                              
            }
        }

        protected void btnVExamen_Click(object sender, EventArgs e)
        {
            Session["IdProfesor"] = HiddenField1.Value;
            Response.Redirect("ExamenesConsul.aspx");
        }

        protected void btnCExamen_Click(object sender, EventArgs e)
        {
            Session["IdProfesor"] = HiddenField1.Value;
            Response.Redirect("ExamenABC.aspx");
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Session["TablaUser"] = null;
            Session["TablaProfesor"] = null;
            Session["TablaAlumno"] = null;
            Session["Estado"] = null;
            Session["Estado2"] = null;
            Response.Redirect("login.aspx");
        }

        protected void btnImprimirExa_Click(object sender, EventArgs e)
        {
            Session["IdProfesor"] = HiddenField1.Value;
            Response.Redirect("GenerarReporte.aspx");
        }

        protected void btnECuentas_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["TablaUser"];
            if (Session["TablaProfesor"] == null)
            {
                Session["Estado2"] = "Alumno";
                Session["Estado"] = "C";
                Session["Tabla"] = dt;
            }
            Response.Redirect("cuentasABC.aspx");
        }

        protected void btnDash_Click(object sender, EventArgs e)
        {
            if (Session["TablaProfesor"] != null)
            {
                Response.Redirect("Home.aspx");
            }
            else
            {
                Response.Redirect("homeFront.aspx");
            }
        }

        protected void btnEAlumnos_Click(object sender, EventArgs e)
        {
            Response.Redirect("AlumnosABC.aspx");
        }

        protected void btnEGrados_Click(object sender, EventArgs e)
        {            
            Response.Redirect("GradoABC.aspx");
        }

        protected void btnEMaterias_Click(object sender, EventArgs e)
        {
            Response.Redirect("MateriaABC.aspx");
        }

        protected void btnVAlumnos_Click(object sender, EventArgs e)
        {
            Response.Redirect("AlumConsul.aspx");
        }

        protected void btnVCuentas_Click(object sender, EventArgs e)
        {
            Response.Redirect("cuentasConsul.aspx");
        }

        protected void btnVGrados_Click(object sender, EventArgs e)
        {
            Response.Redirect("GyGconsul.aspx");
        }

        protected void btnVMaterias_Click(object sender, EventArgs e)
        {
            Response.Redirect("MaterConsul.aspx");
        }

        protected void btnRevision_Click(object sender, EventArgs e)
        {
            Response.Redirect("homeFront.aspx");
        }

        protected void btnPerfil_Click(object sender, EventArgs e)
        {
            Session["Estado"] = "Sig";
            Response.Redirect("singUpA.aspx");
            
        }

    }
}