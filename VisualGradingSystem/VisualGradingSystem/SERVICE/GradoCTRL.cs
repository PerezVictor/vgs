﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VisualGradingSystem.DAO;

namespace VisualGradingSystem.SERVICE
{
    public class GradoCTRL
    {
        GradoDAO oGradoDAO = new GradoDAO();

        public DataSet consultar(object obj)
        {
            DataSet resultado = oGradoDAO.consultar(obj);
            return resultado;
        }
        public DataSet consultarCount()
        {
            DataSet resultado = oGradoDAO.consultarCount();
            return resultado;
        }
        public DataSet consultarDD()
        {
            DataSet resultado = oGradoDAO.consultarDD();
            return resultado;
        }
        public int crear(object obj)
        {
            int resultado = oGradoDAO.crear(obj);
            return resultado;
        }
        public int modificar(object obj)
        {
            int resultado = oGradoDAO.modificar(obj);
            return resultado;
        }
        public int eliminar(object obj)
        {
            int resultado = oGradoDAO.eliminar(obj);
            return resultado;
        }
    }
}