﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using VisualGradingSystem.DAO;

namespace VisualGradingSystem.SERVICE
{
    public class CuentaCTRL
    {
        CuentaDAO oCuentaDAO = new CuentaDAO();

        public DataSet consultar(object obj)
        {
            DataSet resultado = oCuentaDAO.consultar(obj);
            return resultado;
        }
        public DataSet consultarLike(object obj)
        {
            DataSet resultado = oCuentaDAO.consultarLike(obj);
            return resultado;
        }
        public DataSet consultarMt()
        {
            DataSet resultado = oCuentaDAO.consultarMt();
            return resultado;
        }
        public DataSet consultarDD(int id)
        {
            DataSet resultado = oCuentaDAO.consultarDD(id);
            return resultado;
        }
        public int crear(object obj)
        {
            int resultado = oCuentaDAO.crear(obj);
            return resultado;
        }
        public int modificar(object obj)
        {
            int resultado = oCuentaDAO.modificar(obj);
            return resultado;
        }
        public int eliminar(object obj)
        {
            int resultado = oCuentaDAO.eliminar(obj);
            return resultado;
        }
    }
}