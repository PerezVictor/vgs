﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VisualGradingSystem.DAO;

namespace VisualGradingSystem.SERVICE
{
    public class ProfesorCTRL
    {
        ProfesorDAO oProfesorDAO = new ProfesorDAO();

        public DataSet consultar(object obj)
        {
            DataSet resultado = oProfesorDAO.consultar(obj);
            return resultado;
        }
        public DataSet consultarDD()
        {
            DataSet resultado = oProfesorDAO.consultarDD();
            return resultado;
        }
        public int crear(object obj)
        {
            int resultado = oProfesorDAO.crear(obj);
            return resultado;
        }
        public int modificar(object obj)
        {
            int resultado = oProfesorDAO.modificar(obj);
            return resultado;
        }
        public int eliminar(object obj)
        {
            int resultado = oProfesorDAO.eliminar(obj);
            return resultado;
        }
    }
}