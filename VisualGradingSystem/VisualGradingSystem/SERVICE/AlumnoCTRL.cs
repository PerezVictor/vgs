﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using VisualGradingSystem.DAO;

namespace VisualGradingSystem.SERVICE
{
    public class AlumnoCTRL
    {
        AlumnoDAO oAlumnoDAO = new AlumnoDAO();

        public DataSet consultar(object obj)
        {
            DataSet resultado = oAlumnoDAO.consultar(obj);
            return resultado;
        }
        public DataSet consultarCount()
        {
            DataSet resultado = oAlumnoDAO.consultarCount();
            return resultado;
        }
        public int crear(object obj)
        {
            int resultado = oAlumnoDAO.crear(obj);
            return resultado;
        }
        public int modificar(object obj)
        {
            int resultado = oAlumnoDAO.modificar(obj);
            return resultado;
        }
        public int eliminar(object obj)
        {
            int resultado = oAlumnoDAO.eliminar(obj);
            return resultado;
        }
    }
}