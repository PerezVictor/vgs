﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using VisualGradingSystem.DAO;

namespace VisualGradingSystem.SERVICE
{
    public class ExamenCTRL
    {
        ExamenDAO oExamenDAO = new ExamenDAO();

        public DataSet consultar(object obj)
        {
            DataSet resultado = oExamenDAO.consultar(obj);
            return resultado;
        }
        public DataSet consultarDD(int IdProfesor)
        {
            DataSet resultado = oExamenDAO.consultarDD(IdProfesor);
            return resultado;
        }
        public DataSet consultarDD2(int IdProfesor)
        {
            DataSet resultado = oExamenDAO.consultarDD2(IdProfesor);
            return resultado;
        } 
        public DataSet consultarCount(string IdProfesor)
        {
            DataSet resultado = oExamenDAO.consultarCount(IdProfesor);
            return resultado;
        }
        public DataSet consultarPromedios(int id)
        {
            DataSet resultado = oExamenDAO.consultarPromedios(id);
            return resultado;
        }
        public DataSet consultarReporte(string IdProfesor, string IdExamen, string IdGrado)
        {
            DataSet resultado = oExamenDAO.consultarReporte(IdProfesor, IdExamen, IdGrado);
            return resultado;
        }
        public int crear(object obj)
        {
            int resultado = oExamenDAO.crear(obj);
            return resultado;
        }
        public int modificar(object obj)
        {
            int resultado = oExamenDAO.modificar(obj);
            return resultado;
        }
        public int eliminar(object obj)
        {
            int resultado = oExamenDAO.eliminar(obj);
            return resultado;
        }
    }
}