﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VisualGradingSystem.DAO;

namespace VisualGradingSystem.SERVICE
{
    public class MateriaCTRL
    {
        MateriaDAO oMateriaDAO = new MateriaDAO();

        public DataSet consultar(object obj)
        {
            DataSet resultado = oMateriaDAO.consultar(obj);
            return resultado;
        }        
        public DataSet consultarDD()
        {
            DataSet resultado = oMateriaDAO.consultarDD();
            return resultado;
        }
        public int crear(object obj)
        {
            int resultado = oMateriaDAO.crear(obj);
            return resultado;
        }
        public int modificar(object obj)
        {
            int resultado = oMateriaDAO.modificar(obj);
            return resultado;
        }
        public int eliminar(object obj)
        {
            int resultado = oMateriaDAO.eliminar(obj);
            return resultado;
        }
    }
}