﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VisualGradingSystem.DAO;

namespace VisualGradingSystem.SERVICE
{
    public class PreguntaCTRL
    {
        PreguntaDAO oPreguntaDAO = new PreguntaDAO();

        public DataSet consultar(object obj)
        {
            DataSet resultado = oPreguntaDAO.consultar(obj);
            return resultado;
        }
        public DataSet consultarR(int idAlumno, int idExamen)
        {
            DataSet resultado = oPreguntaDAO.consultarR(idAlumno, idExamen);
            return resultado;
        }
        public int crear(object obj)
        {
            int resultado = oPreguntaDAO.crear(obj);
            return resultado;
        }
        public int modificar(object obj)
        {
            int resultado = oPreguntaDAO.modificar(obj);
            return resultado;
        }
        public int modificarPre(int idPregunta)
        {
            int resultado = oPreguntaDAO.modificarPre(idPregunta);
            return resultado;
        }
        public int eliminar(object obj)
        {
            int resultado = oPreguntaDAO.eliminar(obj);
            return resultado;
        }
    }
}